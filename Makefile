TARGET := fcg
SOURCES := main.cpp characterHelper.c
LIBS := -liconv -lcurl
CFLAGS := -O2 -g
CXXFLAGS := -std=c++11 -I./ext/jsoncons/include -I./ext/glaze/include

OBJS := $(SOURCES:.c=.o)
OBJS := $(OBJS:.cpp=.o)

.PHONY: all
all: $(TARGET)
$(TARGET) : $(OBJS)
	$(CXX) $(CFLAGS) $(CXXFLAGS) -o $(TARGET) $^ $(LIBS)

.c.o:
	$(CC) $(CFLAGS) -c $<

.cpp.o:
	$(CXX) $(CFLAGS) $(CXXFLAGS) -c $<

.PHONY: clean
clean:
	rm -f $(OBJS) $(TARGET)

dependencies:
	$(CC) -MM $(SOURCES) > dependencies

include dependencies
