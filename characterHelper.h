#ifndef CHARACTER_HELPER_H
#define CHARACTER_HELPER_H
#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif

size_t convertUTF8ToUTF32(uint32_t *utf32, const char *utf8, size_t length);
size_t normalizeUTF32toUTF8(char *utf8, const uint32_t *utf32, size_t length);
void getUTF8SequenceFromUnicode(char **utf8ptr, const uint32_t utf32char);
char *convertSJISToUTF8(const char *inbuf, size_t length);

#ifdef __cplusplus
}
#endif
#endif
