#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include <windows.h>
#else
#include <errno.h>
#include <iconv.h>
#endif
#include "characterHelper.h"

static inline int getUnescapedUnicodeFromASCII(const char **ptr)
{
	if(!strncmp(*ptr,"&lt;",4)) {
		*ptr += 3;
		return '<';
	}
	else if(!strncmp(*ptr,"&gt;",4)) {
		*ptr += 3;
		return '>';
	}
	else if(!strncmp(*ptr,"&amp;",5)) {
		*ptr += 4;
		return '&';
	}
	else if(!strncmp(*ptr,"&quot;",6)) {
		*ptr += 5;
		return '"';
	}
	else if(!strncmp(*ptr,"&apos;",6)) {
		*ptr += 5;
		return '\'';
	}
	else if(!strncmp(*ptr,"&#x",3)) {
		char *end;
		unsigned long c = strtoul(*ptr+3,&end,16);
		if(*end == ';') {
			*ptr = end;
			return c;
		}
	}
	else if(!strncmp(*ptr,"&#",2)) {
		char *end;
		unsigned long c = strtoul(*ptr+2,&end,10);
		if(*end == ';') {
			*ptr = end;
			return c;
		}
	}
	return **ptr;
}

size_t convertUTF8ToUTF32(uint32_t *utf32, const char *utf8, size_t length)
{
	size_t words = 0;
	int subsequent = 0;
	const char *end = utf8+length;
	for(;utf8<end;utf8++) {
		unsigned char c = *utf8;
		if(c < 0x80) {
			if(subsequent) fprintf(stderr,"bad utf-8 encoding\n");
			else {
				utf32[words++] = getUnescapedUnicodeFromASCII(&utf8);
			}
		}
		else if(c < 0xc0) {
			if(!subsequent) fprintf(stderr,"bad utf-8 encoding\n");
			else {
				utf32[words] = (utf32[words] << 6) | (c & 0x3f);
				subsequent--;
				if(!subsequent) words++;
			}
		}
		else if(c < 0xe0) {
			if(subsequent) fprintf(stderr,"bad utf-8 encoding\n");
			else {
				utf32[words] = c & 0x1f;
				subsequent = 1;
			}
		}
		else if(c < 0xf0) {
			if(subsequent) fprintf(stderr,"bad utf-8 encoding\n");
			else {
				utf32[words] = c & 0x0f;
				subsequent = 2;
			}
		}
		else if(c < 0xf8) {
			if(subsequent) fprintf(stderr,"bad utf-8 encoding\n");
			else {
				utf32[words] = c & 0x07;
				subsequent = 3;
			}
		}
		else if(c < 0xfc) {
			if(subsequent) fprintf(stderr,"bad utf-8 encoding\n");
			else {
				utf32[words] = c & 0x03;
				subsequent = 4;
			}
		}
		else {
			if(subsequent) fprintf(stderr,"bad utf-8 encoding\n");
			else {
				utf32[words] = c & 0x01;
				subsequent = 5;
			}
		}
	}
	utf32[words] = 0;
	return words;
}

void getUTF8SequenceFromUnicode(char **utf8ptr, const uint32_t utf32char)
{
	char *utf8 = *utf8ptr;
	if(utf32char < 0x80) {
		if(utf32char == '<') {
			strcpy(utf8,"\\u003c");
			utf8 += 6;
		}
		else if(utf32char == '>') {
			strcpy(utf8,"\\u003e");
			utf8 += 6;
		}
		else if(utf32char == '&') {
			strcpy(utf8,"\\u0026");
			utf8 += 6;
		}
		else if(utf32char == '"') {
			strcpy(utf8,"\\u0022");
			utf8 += 6;
		}
		else if(utf32char == '\'') {
			strcpy(utf8,"\\u0027");
			utf8 += 6;
		}
		else if(utf32char == '\\') {
			strcpy(utf8,"\\\\");
			utf8 += 2;
		}
		else if(utf32char > 0 && utf32char < 0x20) {
			*utf8++ = 0x20;
		}
		else *utf8++ = utf32char;
	}
	else if(utf32char < 0x800) {
		*utf8++ = 0xc0 | ((utf32char & 0x7c0) >> 6);
		*utf8++ = 0x80 | (utf32char & 0x3f);
	}
	else if(utf32char < 0x10000) {
		*utf8++ = 0xe0 | ((utf32char & 0xf000) >> 12);
		*utf8++ = 0x80 | ((utf32char & 0xfc0) >> 6);
		*utf8++ = 0x80 | (utf32char & 0x3f);
	}
	else if(utf32char < 0x200000) {
		*utf8++ = 0xf0 | ((utf32char & 0x1c0000) >> 18);
		*utf8++ = 0x80 | ((utf32char & 0x3f000) >> 12);
		*utf8++ = 0x80 | ((utf32char & 0xfc0) >> 6);
		*utf8++ = 0x80 | (utf32char & 0x3f);
	}
	else if(utf32char < 0x40000000) {
		*utf8++ = 0xf8 | ((utf32char & 0x3000000) >> 24);
		*utf8++ = 0x80 | ((utf32char & 0xfc0000) >> 18);
		*utf8++ = 0x80 | ((utf32char & 0x3f000) >> 12);
		*utf8++ = 0x80 | ((utf32char & 0xfc0) >> 6);
		*utf8++ = 0x80 | (utf32char & 0x3f);
	}
	else {
		*utf8++ = 0xfc | ((utf32char & 0x40000000) >> 30);
		*utf8++ = 0x80 | ((utf32char & 0x3f000000) >> 24);
		*utf8++ = 0x80 | ((utf32char & 0xfc0000) >> 18);
		*utf8++ = 0x80 | ((utf32char & 0x3f000) >> 12);
		*utf8++ = 0x80 | ((utf32char & 0xfc0) >> 6);
		*utf8++ = 0x80 | (utf32char & 0x3f);
	}
	*utf8ptr = utf8;
}

size_t normalizeUTF32toUTF8(char *utf8, const uint32_t *utf32, size_t length)
{
	char *begin = utf8;
	for(size_t i=0;i<length;i++,utf32++) {
		unsigned int c = *utf32;
		/* 全角英数 -> 半角英数 */
		if(c >= 0xff01 && c <= 0xff5e) c -= 0xfee0;
		/* 。 */
		else if(c == 0xff61) c = 0x3002;
		/* 「 */
		else if(c == 0xff62) c = 0x300c;
		/* 」 */
		else if(c == 0xff63) c = 0x300d;
		/* 、 */
		else if(c == 0xff64) c = 0x3001;
		/* ・ */
		else if(c == 0xff65) c = 0x30fb;
		/* ヲ */
		else if(c == 0xff66) c = 0x30f2;
		/* ァ-ォ */
		else if(c >= 0xff67 && c <= 0xff6b) c = (c - 0xff67)*2 + 0x30a1;
		/* ャ-ョ */
		else if(c >= 0xff6c && c <= 0xff6e) c = (c - 0xff6c)*2 + 0x30e3;
		/* ッ */
		else if(c == 0xff6f) c = 0x30c3;
		/* ー */
		else if(c == 0xff70) c = 0x30fc;
		/* ア-オ */
		else if(c >= 0xff71 && c <= 0xff75) {
			c = (c - 0xff71)*2 + 0x30a2;
			/* ヴ */
			if(c == 0x30a6 && *(utf32+1) == 0xff9e) {
				c = 0x30f4;
				utf32++;
			}
		}
		/* カ-ト */
		else if(c >= 0xff76 && c <= 0xff84) {
			/* カ-チ */
			if(c >= 0xff76 && c <= 0xff81) c = (c - 0xff76)*2 + 0x30ab;
			/* ツ */
			else if(c == 0xff82) c = 0x30c4;
			/* テト */
			else if(c >= 0xff83 && c <= 0xff84) c = (c - 0xff83)*2 + 0x30c6;
			/* 濁点結合 */
			if(*(utf32+1) == 0xff9e) {
				c++;
				utf32++;
			}
		}
		/* ナ-ノ */
		else if(c >= 0xff85 && c <= 0xff89) c = (c - 0xff85) + 0x30ca;
		/* ハ-ホ */
		else if(c >= 0xff8a && c <= 0xff8e) {
			c = (c - 0xff8a)*3 + 0x30cf;
			/* 濁点結合 */
			if(*(utf32+1) == 0xff9e) {
				c++;
				utf32++;
			}
			/* 半濁点結合 */
			else if(*(utf32+1) == 0xff9f) {
				c+=2;
				utf32++;
			}
		}
		/* マ-モ */
		else if(c >= 0xff8f && c <= 0xff93) c = (c - 0xff8f) + 0x30de;
		/* ヤ-ヨ */
		else if(c >= 0xff94 && c <= 0xff96) c = (c - 0xff94)*2 + 0x30e4;
		/* ラ-ロ */
		else if(c >= 0xff97 && c <= 0xff9b) c = (c - 0xff97) + 0x30e9;
		/* ワ */
		else if(c == 0xff9c) {
			c = 0x30ef;
			/* 濁点結合 */
			if(*(utf32+1) == 0xff9e) {
				c = 0x30f7;
				utf32++;
			}
		}
		/* ン */
		else if(c == 0xff9d) c = 0x30f3;
		/* 濁点半濁点 */
		else if(c >= 0xff9e && c <= 0xff9f) c = (c - 0xff9e) + 0x309b;
		
		/* 大文字 -> 小文字 */
		if(c >= 0x41 && c <= 0x5a) c += 0x20;
		getUTF8SequenceFromUnicode(&utf8,c);
	}
	*utf8 = 0;
	return (utf8 - begin);
}

char *convertSJISToUTF8(const char *inbuf, size_t length)
{
#ifdef _WIN32
	int wideLength = MultiByteToWideChar(932, 0, inbuf, length, NULL, 0);
	if (wideLength == 0) return NULL;
	LPWSTR inWideStr = (LPWSTR)malloc(wideLength * sizeof(WCHAR));
	wideLength = MultiByteToWideChar(932, 0, inbuf, length, inWideStr, wideLength);
	int outLength = WideCharToMultiByte(CP_UTF8, 0, inWideStr, wideLength, NULL, 0, NULL, NULL);
	char *outbuf = calloc(outLength+1, 1);
	WideCharToMultiByte(CP_UTF8, 0, inWideStr, wideLength, outbuf, outLength, NULL, NULL);
	free(inWideStr);
	return outbuf;
#else
	size_t inbytesleft, outbytesleft;
	inbytesleft = length;
	outbytesleft = inbytesleft*3+1;
	char *outbuf = (char *)malloc(outbytesleft);
	char *outbuf_p = outbuf;
	iconv_t cd = iconv_open("UTF-8", "CP932");
	while(1) {
		int ret = iconv(cd, (char **)&inbuf, &inbytesleft, &outbuf_p, &outbytesleft);
		if(ret != -1) {
			break;
		}
		else if (errno == EILSEQ) {
			inbuf += 2;
			inbytesleft -= 2;
		}
		else break;
	}
	iconv_close(cd);
	if(!outbuf_p) {
		free(outbuf);
		return NULL;
	}
	*outbuf_p = 0;
	return outbuf;
#endif
}
