#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <stdint.h>
#include <time.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <curl/curl.h>
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include <algorithm>
#include <tuple>
#include <deque>
#include <chrono>
#include <thread>
#include <fstream>
#include <iostream>
#if (__cplusplus < 202002L) || !defined(USE_GLAZE)
#include <jsoncons/json.hpp>
#else
#include <glaze/glaze.hpp>
#endif
#ifdef _WIN32
#include <windows.h>
#define localtime_r(a, b) localtime_s(b, a)
#endif
#include "characterHelper.h"

/*
./fcg と引数なしで起動すると巡回モードになりHTMLの自動生成を繰り返します。巡回する板の指定はboards.txtで行います。
./fcg may b のように引数を2つ指定すると http://may.2chan.net/b/ の掲示板に対してカタログを生成して終了します。
*/

/* ---------------- 設定ここから ---------------- */

/* カタログHTMLを出力するディレクトリのデフォルトのパス */
static const char *htmlPath = ".";
/* 巡回する板を定義するファイルのデフォルトのパス */
static const char *boardsTxtPath = "boards.txt";
/* NGワードを定義するファイルのデフォルトのパス */
static const char *NGTxtPath = "ngword.txt";
/* カタログの1行あたりのスレッド数 */
static const int threadsPerRow = 12;
/* カタログに表示する最大スレッド数 */
static int maxThreadCount = 700;

/* ---------------- 設定ここまで ---------------- */

#ifndef MODERN_SCRIPT_FILENAME
#define MODERN_SCRIPT_FILENAME "script_modern.js"
#endif

static bool checkCertificate = true;
static volatile sig_atomic_t quit;
static std::string scriptQueryString;
static std::string modernScriptQueryString;
static std::string cssQueryString;

struct FCGBoard {
	const char *name;
	const char *server;
	const char *board;
	const char *baseURL;
	const char *scheme;
	int interval;
	int lastVisited;
	unsigned int lengthThreshold;
	CURL *curl;
	unsigned int detectGarbage;
	FCGBoard() :
		name(),
		server(),
		board(),
		baseURL(),
		scheme(),
		interval(),
		lastVisited(),
		lengthThreshold(),
		detectGarbage()
	{
		curl = curl_easy_init();
	}
	FCGBoard(const char *server_, const char *board_, const char *name_, bool secure) :
		interval(),
		lastVisited(),
		lengthThreshold(50),
		detectGarbage()
	{
		char tmp[1024];
		name = strdup(name_);
		server = strdup(server_);
		board = strdup(board_);
		if(secure) {
			scheme = "https";
			snprintf(tmp, 1024, "https://%s.2chan.net/%s/", server_, board_);
		} else {
			scheme = "http";
			snprintf(tmp, 1024, "http://%s.2chan.net/%s/", server_, board_);
		}
		baseURL = strdup(tmp);
		curl = curl_easy_init();
	}
	~FCGBoard()
	{
		if(name) free((void*)name);
		if(server) free((void*)server);
		if(board) free((void*)board);
		if(baseURL) free((void*)baseURL);
		curl_easy_cleanup(curl);
	}
	std::string identifier(void)
	{
		std::string str(server);
		str += "/";
		str += board;
		return str;
	}
};

typedef std::unique_ptr<FCGBoard> PFCGBoard;
typedef std::vector<PFCGBoard> FCGBoardsArray;
static FCGBoardsArray boards;

static std::vector<std::string> ngWords;

struct FCGThread
{
	std::string path;
	std::string image;
	std::string normalizedComment;
	std::string visibleComment;
	unsigned int length;
	unsigned long key;
	unsigned long created;
	int tag;
	int increment;
	bool banned;
	int heat;
	bool fullyAnalyzed;
	unsigned int flags;
	std::deque<std::tuple<time_t, unsigned int> > lengthHistory;
	float heat1H;
	enum Flags {
		FLAG_ID_SHOWN = 1,
		FLAG_IP_SHOWN = 2,
		FLAG_ID_DISCLOSED = 4,
		FLAG_ISOLATED = 8,
		FLAG_DELETED_BUT_REMAINS = 16,
		FLAG_NOT_GARBAGE = 32,
		FLAG_GARBAGE = 64,
		FLAG_BIRTH_UNKNOWN = 128,
	};
	FCGThread() : length(0), key(0), created(0), tag(0), increment(-1), banned(false), heat(0), fullyAnalyzed(false), flags(0), heat1H(-1) {}
	~FCGThread() {}

	void setPath(const char *ptr, size_t size)
	{
		path.assign(ptr,size);
		
		for(;size>0;size--) {
			if(ptr[size-1] == '/') break;
		}
		key = strtoul(ptr+size,NULL,10);
	}
	void setImage(const char *ptr, size_t size)
	{
		image.assign(ptr,size);
		
		for(;size>0;size--) {
			if(ptr[size-1] == '/') break;
		}
		created = strtoull(ptr+size,NULL,10) / 1000;
	}
	void setComment(const char *ptr, ssize_t size)
	{
		if(size == 0) {
			normalizedComment.clear();
			visibleComment.clear();
			return;
		}
		else if(size < 0) size = strlen(ptr);
		uint32_t *utf32 = new uint32_t[size+1];
		char *normalized = new char[size*2+1];
		char *visible = new char[128];
		char *outptr = visible;
		int words = convertUTF8ToUTF32(utf32, ptr, size);
		normalizeUTF32toUTF8(normalized, utf32, words);
		normalizedComment.assign(normalized);
		
		for(int i=0,visibleLength=0;i<words && visibleLength<8;i++) {
			unsigned int c = utf32[i];
			if(c == '<') {
				strcpy(outptr,"&lt;");
				outptr += 4;
				visibleLength++;
			}
			else if(c == '>') {
				strcpy(outptr,"&gt;");
				outptr += 4;
				visibleLength++;
			}
			else if(c == '&') {
				strcpy(outptr,"&amp;");
				outptr += 5;
				visibleLength++;
			}
			else if(c == '"') {
				strcpy(outptr,"&quot;");
				outptr += 6;
				visibleLength++;
			}
			else if(c == '\'') {
				strcpy(outptr,"&apos;");
				outptr += 6;
				visibleLength++;
			}
			else if(c == '\\') {
				strcpy(outptr,"&yen;");
				outptr += 5;
				visibleLength++;
			}
			else {
				getUTF8SequenceFromUnicode(&outptr,c);
				if(c <= 0xff) {
					visibleLength++;
				}
				else if(c >= 0xff61 && c <= 0xff9f) visibleLength++;
				else if(c >= 0xd800 && c <= 0xd8ff) continue;
				else visibleLength+=2;
			}
		}
		*outptr = 0;
		visibleComment.assign(visible);
		delete [] utf32;
		delete [] normalized;
		delete [] visible;
	}
	bool isGarbage() {
		return false;
	}
	float getHeat1H() {
		if (heat1H >= 0) return heat1H;
		time_t now = time(NULL);
		unsigned int lengthOldest = std::get<1>(lengthHistory.back());
		int duration = now - std::get<0>(lengthHistory.back());
		for (auto it = lengthHistory.begin(); it != lengthHistory.end(); ++it) {
			if (now - std::get<0>(*it) > 60*60) {
				lengthOldest = std::get<1>(*it);
				lengthHistory.erase(it, lengthHistory.end());
				lengthHistory.emplace_back(now-60*60, lengthOldest);
				break;
			}
		}
		heat1H = length - lengthOldest;
		if (duration < 60) heat1H = 0;
		else if (duration < 3600) heat1H = heat1H/(duration/3600.f);
		return heat1H;
	}
};
#if (__cplusplus < 202002L) || !defined(USE_GLAZE)
JSONCONS_N_MEMBER_TRAITS(FCGThread, 0, path, image, normalizedComment, visibleComment, length, key, created, tag, increment, banned, heat, fullyAnalyzed, flags, lengthHistory)
#else
template <>
struct glz::meta<FCGThread> {
	using T = FCGThread;
	static constexpr auto value = object(
		&T::path,
		&T::image,
		&T::normalizedComment,
		&T::visibleComment,
		&T::length,
		&T::key,
		&T::created,
		&T::tag,
		&T::increment,
		&T::banned,
		&T::heat,
		&T::fullyAnalyzed,
		&T::flags,
		&T::lengthHistory
	);
};
#endif

typedef std::shared_ptr<FCGThread> PFCGThread;
typedef std::vector<PFCGThread> FCGThreadsArray;
typedef std::unordered_map<std::string, std::tuple<FCGThreadsArray,FCGThreadsArray,FCGThreadsArray> > FCGThreadsDictionary;

static FCGThreadsDictionary threadsHistory;
static std::deque<std::chrono::system_clock::time_point> jsonRequestHistory;

static void log_printf(const char *format ...)
{
	time_t now = time(NULL);
	struct tm pnow;
	localtime_r(&now, &pnow);
	fprintf(stderr,"[%02d:%02d:%02d] ", pnow.tm_hour, pnow.tm_min, pnow.tm_sec);
	va_list argp;
	va_start(argp, format);
	vfprintf(stderr, format, argp);
	va_end(argp);
	fflush(stderr);
}

static bool threadKeyComparator(const PFCGThread &a, const PFCGThread &b) {
	return a->key > b->key;
}

static bool threadLengthComparator(const PFCGThread &a, const PFCGThread &b) {
	return a->length > b->length;
}

static bool threadHeatComparator(const PFCGThread &a, const PFCGThread &b) {
	return a->heat > b->heat;
}

static bool threadHeat1HComparator(const PFCGThread &a, const PFCGThread &b) {
	return a->getHeat1H() > b->getHeat1H();
}

static size_t dataAvailableCallback(char *buffer, size_t size, size_t nitems, void *userdata)
{
	std::vector<char> *data = reinterpret_cast<std::vector<char> *>(userdata);
	size_t downloaded = size*nitems;
	data->insert(data->end(), buffer, buffer+downloaded);
	return downloaded;
}

static int parseCatalogHTMLOld(const char *html, const PFCGBoard &targetBoard, FCGThreadsArray &threadsTmp)
{
	const char *ptr = html;
	int now = time(NULL);
	int numThread, numPost = 0;
	auto &history = threadsHistory[targetBoard->identifier()];
	auto &threadsPrevious = std::get<0>(history);
	auto &threadsIsolated = std::get<2>(history);
	for(numThread=0;;numThread++) {
		PFCGThread thread(new FCGThread());
		ptr = strstr(ptr,"<td><a href='res/");
		if(!ptr) break;
		const char *tmp;
		const char *end = strstr(ptr,"</td>");
		if(!end) break;
		ptr += 13;
		const char *start = ptr;
		while(*ptr != '\'') ptr++;
		thread->setPath(start, ptr-start);
		for(const auto &prevThread : threadsPrevious) {
			if(thread->key == prevThread->key) {
				thread = prevThread;
				thread->banned = false;
				goto FindLength;
			}
		}
		/* Usually we don't have to check this, but when a catalog overflows
		   threads may be wrongly detected as isolated. */
		for(auto it = threadsIsolated.begin(); it != threadsIsolated.end(); ++it) {
			if(thread->key == (*it)->key) {
				fprintf(stderr, "thread %lu is marked as isolated, but is actually active\n", thread->key);
				thread = *it;
				thread->banned = false;
				thread->flags &= ~FCGThread::FLAG_ISOLATED;
				threadsIsolated.erase(it);
				goto FindLength;
			}
		}
		tmp = strstr(ptr,"<img src='");
		if(tmp && tmp < end) {
			ptr = tmp + 10;
			start = ptr;
			while(*ptr != '\'') ptr++;
			thread->setImage(start, ptr-start);
			tmp = ptr;
			while(*tmp != '/') tmp--;
		} else {
			thread->flags |= FCGThread::FLAG_BIRTH_UNKNOWN;
		}
		tmp = strstr(ptr,"<small>");
		if(tmp && tmp < end) {
			ptr = tmp + 7;
			start = ptr;
			while(*ptr != '<') ptr++;
			thread->setComment(start, ptr-start);
		}
	FindLength:
		tmp = strstr(ptr,"<font size=2>");
		if(tmp && tmp < end) {
			ptr = tmp + 13;
			unsigned int length = atoi(ptr);
			if(thread->increment >= 0) {
				thread->increment = length - thread->length;
			} else {
				thread->increment = 0;
			}
			thread->length = length;
			numPost += 1 + thread->length;
		}
		thread->tag = numThread;
		thread->heat = (int)round(3*3600.0*thread->length/(1+now-thread->created));
		if (thread->increment > 0 || thread->lengthHistory.empty()) {
			thread->lengthHistory.emplace_front(now, thread->length);
		}
		thread->heat1H = -1;
		threadsTmp.push_back(std::move(thread));
	}
	return numThread;
}

static void complementThreadInfoUsingJSON(const PFCGBoard &targetBoard, FCGThreadsArray &threads)
{
	char url[1024];
	snprintf(url, 1024, "%sfutaba.php?mode=json", targetBoard->baseURL);
	std::vector<char> data;
	curl_easy_reset(targetBoard->curl);
	curl_easy_setopt(targetBoard->curl, CURLOPT_URL, url);
	curl_easy_setopt(targetBoard->curl, CURLOPT_NOSIGNAL, 1L);
	curl_easy_setopt(targetBoard->curl, CURLOPT_TIMEOUT, 30L);
	curl_easy_setopt(targetBoard->curl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(targetBoard->curl, CURLOPT_ACCEPT_ENCODING, "");
	curl_easy_setopt(targetBoard->curl, CURLOPT_WRITEFUNCTION, dataAvailableCallback);
	curl_easy_setopt(targetBoard->curl, CURLOPT_WRITEDATA, &data);
	if (!checkCertificate) {
		curl_easy_setopt(targetBoard->curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(targetBoard->curl, CURLOPT_SSL_VERIFYHOST, 0L);
	}
	auto now = std::chrono::system_clock::now();
	if (jsonRequestHistory.size()) {
		do {
			if (std::chrono::duration_cast<std::chrono::milliseconds>(now - jsonRequestHistory[0]) > std::chrono::seconds(5)) jsonRequestHistory.pop_front();
			else break;
		} while (jsonRequestHistory.size());
		if (jsonRequestHistory.size() >= 5) {
			jsonRequestHistory.pop_front();
			auto durationToSleep = std::chrono::seconds(5) - std::chrono::duration_cast<std::chrono::milliseconds>(now - jsonRequestHistory[0]);
			std::this_thread::sleep_for(durationToSleep); // note: sleep_for() seems to be buggy in VC++; who knows
			now = std::chrono::system_clock::now();
		}
	}
	jsonRequestHistory.push_back(std::move(now));
	CURLcode ret = curl_easy_perform(targetBoard->curl);
	if (ret != CURLE_OK) {
		log_printf("curl error: %s - %s\n", curl_easy_strerror(ret), url);
		return;
	} else {
		long statusCode = 0;
		curl_easy_getinfo(targetBoard->curl, CURLINFO_RESPONSE_CODE, &statusCode);
		if (statusCode != 200) {
			log_printf("error: server returned %ld instead of 200 - %s\n", statusCode, url);
			return;
		}
	}
	data.push_back(0);
#if (__cplusplus < 202002L) || !defined(USE_GLAZE)
	jsoncons::json root;
	try {
		root = jsoncons::json::parse(data.data());
	}
	catch(...) {
		log_printf("JSON: parse error\n");
		return;
	}
	
	if (!root.is_object()) {
		log_printf("JSON: root value is not an object\n");
		return;
	}
	
	const auto &threadInfo = root.at_or_null("res");
	if (!threadInfo.is_object()) {
		log_printf("JSON: cannot find thread information\n");
		return;
	}
	for (const auto &thread : threads) {
		const auto &threadObj = threadInfo.at_or_null(std::to_string(thread->key));
		if (!threadObj.is_object()) continue;
		const auto &id = threadObj.at_or_null("id");
		const auto &ext = threadObj.at_or_null("ext");
		if (id.is_string()) {
			if (!strncmp(id.as<std::string>().c_str(), "ID:", 3)) thread->flags |= FCGThread::FLAG_ID_DISCLOSED;
		}
		if (!thread->image.empty() && ext.is_string() && ext.as<jsoncons::string_view>().empty()) {
			thread->flags |= FCGThread::FLAG_DELETED_BUT_REMAINS;
		}
		if (thread->fullyAnalyzed) continue;
		const auto &mail = threadObj.at_or_null("email");
		if (mail.is_string()) {
			const auto &str = mail.as<std::string>();
			if (!strcasecmp(str.c_str(), "id表示")) thread->flags |= FCGThread::FLAG_ID_SHOWN;
			else if (!strcasecmp(str.c_str(),"ip表示")) thread->flags |= FCGThread::FLAG_IP_SHOWN;
		}
		if (thread->flags & FCGThread::FLAG_BIRTH_UNKNOWN) {
			const auto &tim = threadObj.at_or_null("tim");
			if (tim.is_string() || tim.is_number()) {
				double birth = tim.as<double>();
				thread->created = (unsigned long)(birth / 1000);
				thread->flags &= ~FCGThread::FLAG_BIRTH_UNKNOWN;
			}
		}
		thread->fullyAnalyzed = true;
	}
	auto &history = threadsHistory[targetBoard->identifier()];
	FCGThreadsArray &threadsDeleted = std::get<1>(history);
	FCGThreadsArray &threadsIsolated = std::get<2>(history);
	for (auto it = threadsIsolated.rbegin(); it != threadsIsolated.rend(); it++) {
		const auto &thread = *it;
		thread->flags |= FCGThread::FLAG_ISOLATED;
		const auto &threadObj = threadInfo.at_or_null(std::to_string(thread->key));
		if (threadObj.is_null()) { //deleted after isolation
			threadsDeleted.insert(threadsDeleted.begin(),thread);
			threadsIsolated.erase(std::next(it).base());
			continue;
		}
		if (!threadObj.is_object()) continue;
		const auto &id = threadObj.at_or_null("id");
		if (id.is_string()) {
			if (!strncmp(id.as<std::string>().c_str(), "ID:", 3)) thread->flags |= FCGThread::FLAG_ID_DISCLOSED;
		}
	}
#else
	glz::json_t root;
	auto err = glz::read_json(root, data.data());
	if (err) {
		log_printf("JSON: parse error\n");
		return;
	}

	if (!root.is_object()) {
		log_printf("JSON: root value is not an object\n");
		return;
	}

	auto &threadInfo = root.at("res");
	if (!threadInfo.is_object()) {
		log_printf("JSON: cannot find thread information\n");
		return;
	}
	for (const auto &thread : threads) {
		auto &threadObj = threadInfo.at(std::to_string(thread->key));
		if (!threadObj.is_object()) continue;
		auto &id = threadObj.at("id");
		auto &ext = threadObj.at("ext");
		if (id.is_string()) {
			if (id.get_string().starts_with("ID:")) thread->flags |= FCGThread::FLAG_ID_DISCLOSED;
		}
		if (!thread->image.empty() && ext.is_string() && ext.get_string().empty()) {
			thread->flags |= FCGThread::FLAG_DELETED_BUT_REMAINS;
		}
		if (thread->fullyAnalyzed) continue;
		auto &mail = threadObj.at("email");
		if (mail.is_string()) {
			const auto &str = mail.get_string();
			if (!strcasecmp(str.c_str(), "id表示")) thread->flags |= FCGThread::FLAG_ID_SHOWN;
			else if (!strcasecmp(str.c_str(),"ip表示")) thread->flags |= FCGThread::FLAG_IP_SHOWN;
		}
		if (thread->flags & FCGThread::FLAG_BIRTH_UNKNOWN) {
			auto &tim = threadObj.at("tim");
			if (tim.is_string() || tim.is_number()) {
				double birth = tim.is_string() ? std::stod(tim.get_string()) : tim.get_number();
				thread->created = (unsigned long)(birth / 1000);
				thread->flags &= ~FCGThread::FLAG_BIRTH_UNKNOWN;
			}
		}
		thread->fullyAnalyzed = true;
	}
	auto &history = threadsHistory[targetBoard->identifier()];
	FCGThreadsArray &threadsDeleted = std::get<1>(history);
	FCGThreadsArray &threadsIsolated = std::get<2>(history);
	for (auto it = threadsIsolated.rbegin(); it != threadsIsolated.rend(); it++) {
		const auto &thread = *it;
		thread->flags |= FCGThread::FLAG_ISOLATED;
		auto &threadObj = threadInfo.at(std::to_string(thread->key));
		if (!threadObj) { //deleted after isolation
			threadsDeleted.insert(threadsDeleted.begin(),thread);
			threadsIsolated.erase(std::next(it).base());
			continue;
		}
		if (!threadObj.is_object()) continue;
		auto &id = threadObj.at("id");
		if (id.is_string()) {
			if (id.get_string().starts_with("ID:")) thread->flags |= FCGThread::FLAG_ID_DISCLOSED;
		}
	}
#endif
}

static int retreiveThreadsFromCatalog(const PFCGBoard &targetBoard, FCGThreadsArray &threads, FCGThreadsArray &threadsSlow, FCGThreadsArray &threadsGarbage)
{
	FCGThreadsArray threadsTmp;
	char url[1024],cookie[64];
	snprintf(url, 1024, "%sfutaba.php?mode=cat", targetBoard->baseURL);
	snprintf(cookie, 64, "cxyl=%dx50x256x0x0", maxThreadCount/50);
	std::vector<char> html;
	curl_easy_reset(targetBoard->curl);
	curl_easy_setopt(targetBoard->curl, CURLOPT_URL, url);
	curl_easy_setopt(targetBoard->curl, CURLOPT_COOKIE, cookie);
	curl_easy_setopt(targetBoard->curl, CURLOPT_NOSIGNAL, 1L);
	curl_easy_setopt(targetBoard->curl, CURLOPT_TIMEOUT, 30L);
	curl_easy_setopt(targetBoard->curl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(targetBoard->curl, CURLOPT_ACCEPT_ENCODING, "");
	curl_easy_setopt(targetBoard->curl, CURLOPT_WRITEFUNCTION, dataAvailableCallback);
	curl_easy_setopt(targetBoard->curl, CURLOPT_WRITEDATA, &html);
	if(!checkCertificate) {
		curl_easy_setopt(targetBoard->curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(targetBoard->curl, CURLOPT_SSL_VERIFYHOST, 0L);
	}
	CURLcode ret = curl_easy_perform(targetBoard->curl);
	if(ret != CURLE_OK) {
		log_printf("curl error: %s - %s\n", curl_easy_strerror(ret), url);
		return -1;
	} else {
		long statusCode = 0;
		curl_easy_getinfo(targetBoard->curl, CURLINFO_RESPONSE_CODE, &statusCode);
		if(statusCode != 200) {
			log_printf("error: server returned %ld instead of 200 - %s\n", statusCode, url);
			return -1;
		}
	}
	
	char *outbuf = convertSJISToUTF8(html.data(),html.size());
	if(!outbuf) {
		log_printf("error: failed to convert catalog data to UTF-8 - %s\n", url);
		return -1;
	}
	
	int numThread = parseCatalogHTMLOld(outbuf, targetBoard, threadsTmp);
	if (numThread) complementThreadInfoUsingJSON(targetBoard, threadsTmp);
	
	std::sort(threadsTmp.begin(), threadsTmp.end(), threadKeyComparator);
	if(numThread) {
		float threshold, avg = 0;
		int now = time(NULL);
		long long created = now;
		for(const auto &thread : threadsTmp) {
			if(thread->created) created = thread->created;
			else thread->created = created;
			avg += (1.0f+thread->length)/(0.01f+now-created);
		}
		threshold = fminf(1.5f*60*avg/numThread, 0.7f);
		for(auto &&thread : threadsTmp) {
			bool skip = false;
			for(const auto &word : ngWords) {
				if(strstr(thread->normalizedComment.c_str(), word.c_str())) {
					skip = true;
					thread->banned = true;
					numThread--;
					break;
				}
			}
			if(skip) continue;
			if (targetBoard->detectGarbage) {
				// do something here
			}
			float speed = (float)(1.0f+thread->length)/(now-thread->created);
			if(thread->length < targetBoard->lengthThreshold && 
			   speed*60*(1.0f+1.0f-1.0f*expf(-1.0*(now-thread->created)/3600.0f)) < threshold) threadsSlow.push_back(std::move(thread));
			else threads.push_back(std::move(thread));
		}
	}
	
	free(outbuf);
	return numThread;
}

static void emitTableHTML(FILE *fp, const PFCGBoard &targetBoard, FCGThreadsArray &arr, bool lazy, bool timed, int tagged)
{
	fprintf(fp,"<table class=\"nom%s\" cellspacing=\"0\">",timed?" sortable":"");
	int i,n;
	time_t now = time(NULL);
	struct tm calendar;
	localtime_r(&now, &calendar);
	int hour = calendar.tm_hour;
	int color = 1;
	int count = arr.size();
	for(i=0,n=0;n<count;n+=threadsPerRow) {
		int rest = count-n;
		FCGThreadsArray subArr(arr.begin()+n, arr.begin()+n+(rest>threadsPerRow?threadsPerRow:rest));
		PFCGThread threadTop = subArr[0];
		//std::sort(subArr.begin(), subArr.end(), threadLengthComparator);
		for(const auto &thread : subArr) {
			if(i==0) {
				fprintf(fp,"<tr>\n");
				if(timed) {
					time_t created = threadTop->created;
					localtime_r(&created, &calendar);
					if(calendar.tm_hour != hour) {
						if(color) color = 0;
						else color = 1;
						hour = calendar.tm_hour;
					}
					fprintf(fp,"<td class=\"rowtim%d\"><div class=\"rowtim\">%d/%d<br>%d:%02d</div></td>\n",color,calendar.tm_mon+1,calendar.tm_mday,calendar.tm_hour,calendar.tm_min);
				}
			}
			int heat = thread->heat;
			int heat_g = 255;
			int heat_b = 238;
			if(heat >= 50) {
				int delta_g = 51.0f*(heat-50.0f)/950.0f;
				int delta_b = 34.0f*(heat-50.0f)/950.0f;
				heat_g -= (int)delta_g;
				heat_b -= (int)delta_b;
				if(heat_g < 204) heat_g = 204;
				if(heat_b < 204) heat_b = 204;
			}
			fprintf(fp,"<td class=\"thread\" data-length=\"%d\"",thread->length);
			if(tagged == 1) fprintf(fp," id=\"t%04d\"",thread->tag);
			else if(tagged == 2) fprintf(fp," id-orig=\"t%04d\"",thread->tag);
			if(heat >= 50) fprintf(fp," heatbg=\"#ff%02x%02x\"",heat_g,heat_b);
			fprintf(fp,"><div class=\"res1\"><a href=\"%s%s\"><span class=\"cat\">",targetBoard->baseURL,thread->path.c_str());
			if(!thread->image.empty()) {
				fprintf(fp,"<img %s=\"%s://%s.2chan.net%s\"",lazy?"class=\"lazy\" data-original":"src",targetBoard->scheme,targetBoard->server,thread->image.c_str());
				if(!lazy && tagged < 2) fprintf(fp," loading=\"lazy\">");
				else fprintf(fp,">");
			} else {
				fprintf(fp,"<span birth='%ld'></span>", thread->created);
			}
			fprintf(fp,"</span>%s<br></a><span class=\"cow\">%d",thread->visibleComment.c_str(),thread->length);
			if(thread->increment > 0) fprintf(fp,"<span class=\"inc\">+%d</span>",thread->increment);
			fprintf(fp,"<span class=\"heat\">&#8203;%d</span></span>",heat);
			if(thread->flags & FCGThread::FLAG_ISOLATED) fprintf(fp,"<span class=\"tmark\">隔</span>");
			if(thread->flags & FCGThread::FLAG_DELETED_BUT_REMAINS) fprintf(fp,"<span class=\"tmark\">削</span>");
			if(thread->flags & FCGThread::FLAG_ID_SHOWN) {
				if(thread->flags & FCGThread::FLAG_ID_DISCLOSED) fprintf(fp,"<span class=\"tmark3\">ID</span>");
				else fprintf(fp,"<span class=\"tmark\">ID</span>");
			}
			else if(thread->flags & FCGThread::FLAG_IP_SHOWN) {
				if(thread->flags & FCGThread::FLAG_ID_DISCLOSED) fprintf(fp,"<span class=\"tmark3\">IP</span>");
				else fprintf(fp,"<span class=\"tmark\">IP</span>");
			}
			else if(thread->flags & FCGThread::FLAG_ID_DISCLOSED) fprintf(fp,"<span class=\"tmark2\">ID</span>");
			fprintf(fp,"</div></td>\n");
			if(i++==threadsPerRow-1) {
				fprintf(fp,"</tr>\n");
				i=0;
			}
		}
	}
	if(i!=0) fprintf(fp,"</tr>");
	fprintf(fp,"</table>\n");
}

static bool isThreadActive(const char *threadURL, CURL *curl)
{
	bool isActive = false;
	curl_easy_reset(curl);
	curl_easy_setopt(curl, CURLOPT_URL, threadURL);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1L);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30L);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "");
	curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
	if(!checkCertificate) {
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	}
	CURLcode ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		log_printf("curl error: %s - %s\n", curl_easy_strerror(ret), threadURL);
	} else {
		long statusCode = 0;
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &statusCode);
		if(statusCode == 200) {
			isActive = true;
		}
	}
	return isActive;
}

static void findThreadsDeleted(const PFCGBoard &targetBoard, FCGThreadsArray &threadsAll, FCGThreadsArray &threadsPrevious, FCGThreadsArray &threadsDeleted, FCGThreadsArray &threadsIsolated)
{
	if(threadsPrevious.size() == 0) return;
	int i=0;
	int idx=0;
	int numThreads = threadsAll.size();
	if(!numThreads) return;
	int lastThreadKey = threadsAll[numThreads-1]->key;
	for(auto it = threadsDeleted.begin(); it != threadsDeleted.end();) {
		const auto &thread = *it;
		if(thread->key < lastThreadKey) it = threadsDeleted.erase(it);
		else it++;
	}
	for(auto it = threadsIsolated.begin(); it != threadsIsolated.end();) {
		const auto &thread = *it;
		if(thread->key < lastThreadKey) it = threadsIsolated.erase(it);
		else {
			thread->tag = numThreads+i++;
			it++;
		}
	}
	for(const auto &thread : threadsPrevious) {
		if(thread->banned) continue;
		if(thread->isGarbage()) continue;
		int num1 = thread->key;
		int num2 = num1+1;
		for(;num2 > num1 && idx < numThreads;) {
			num2 = threadsAll[idx++]->key;
		}
		if(idx > 0) idx--;
		if(num2 == num1) continue;
		if(idx < numThreads*0.9) {
			char url[1024];
			snprintf(url, 1024, "%s%s", targetBoard->baseURL, thread->path.c_str());
			thread->increment = 0;
			if(isThreadActive(url, targetBoard->curl)) {
				thread->tag = numThreads+i++;
				thread->flags |= FCGThread::FLAG_ISOLATED;
				threadsIsolated.insert(threadsIsolated.begin(),thread);
			}
			else threadsDeleted.insert(threadsDeleted.begin(),thread);
		}
	}
}

static int generateCatalog(const PFCGBoard &targetBoard)
{
	const char *server = targetBoard->server;
	const char *board = targetBoard->board;
	FCGThreadsArray threads;
	FCGThreadsArray threadsSlow;
	FCGThreadsArray threadsGarbage;
	FCGThreadsArray threadsTmp, threadsHeat, threadsPop, threadsHeat1H;
	time_t now = time(NULL);
	struct tm calendar;
	localtime_r(&now, &calendar);
	int ret;
	if((ret = retreiveThreadsFromCatalog(targetBoard,threads,threadsSlow,threadsGarbage)) < 0) return -1;
	if(ret == 0) {
		log_printf("No threads found at %s\n", targetBoard->baseURL);
		return 0;
	}
	auto &history = threadsHistory[targetBoard->identifier()];
	FCGThreadsArray threadsAll(threads);
	FCGThreadsArray &threadsPrevious = std::get<0>(history);
	FCGThreadsArray &threadsDeleted = std::get<1>(history);
	FCGThreadsArray &threadsIsolated = std::get<2>(history);
	
	std::copy(threadsSlow.begin(),threadsSlow.end(),std::back_inserter(threadsAll));
	threadsTmp.assign(threadsAll.begin(),threadsAll.end());
	std::sort(threadsTmp.begin(), threadsTmp.end(), threadHeatComparator);
	threadsHeat.assign(threadsTmp.begin(),threadsTmp.begin()+std::min((size_t)threadsPerRow*2,threadsTmp.size()));
	std::sort(threadsTmp.begin(), threadsTmp.end(), threadLengthComparator);
	threadsPop.assign(threadsTmp.begin(),threadsTmp.begin()+std::min((size_t)threadsPerRow*2,threadsTmp.size()));
	std::sort(threadsTmp.begin(), threadsTmp.end(), threadHeat1HComparator);
	threadsHeat1H.assign(threadsTmp.begin(),threadsTmp.begin()+std::min((size_t)threadsPerRow*2,threadsTmp.size()));
	for (auto it = threadsHeat1H.begin(); it != threadsHeat1H.end(); ++it) {
		if ((*it)->heat1H <= 0) {
			threadsHeat1H.erase(it, threadsHeat1H.end());
			break;
		}
	}
	std::copy(threadsGarbage.begin(),threadsGarbage.end(),std::back_inserter(threadsAll));
	std::sort(threadsAll.begin(), threadsAll.end(), threadKeyComparator);
	findThreadsDeleted(targetBoard, threadsAll, threadsPrevious, threadsDeleted, threadsIsolated);
	threadsPrevious.assign(threadsAll.begin(),threadsAll.end());
	
	char file[1024];
	snprintf(file,1024,"%s/%s_%s.html",htmlPath,server,board);
	FILE *fp = fopen(file,"w");
	fprintf(fp,"<!DOCTYPE HTML>\n<html><head>\n");
	fprintf(fp,"<meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\"><title>%s</title>\n",targetBoard->name);
	fprintf(fp,"<base target=\"_blank\">\n");
	fprintf(fp,"<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5/css/all.min.css\" crossorigin=\"anonymous\">\n");
	fprintf(fp,"<link rel=\"stylesheet\" type=\"text/css\" href=\"resources/style.css%s\">\n", cssQueryString.c_str());
	fprintf(fp,R"(<script type="text/javascript">
	if (('localStorage' in window) && (window.localStorage != null)) {
		if (localStorage.getItem('fcg_darkmode') == 1 && localStorage.getItem('fcg_legacy') != 1) {
			document.documentElement.classList.add('dark-theme');
		}
	})" "\n</script></head>\n");
	fprintf(fp,"<body bgcolor=\"#FFFFEE\" text=\"#800000\" link=\"#0000EE\" alink=\"#FF0000\">\n");
	fprintf(fp,"<h1>%s</h1>\n",targetBoard->name);
	if(!targetBoard->interval) fprintf(fp,"<div class=\"finfo\">%d:%02d:%02d 更新<br>%d/%d スレッド</div>\n",calendar.tm_hour,calendar.tm_min,calendar.tm_sec,ret,maxThreadCount);
	else {
		fprintf(fp,"<div class=\"finfo\"><table><tr><td>%d:%02d:%02d 更新</td>",calendar.tm_hour,calendar.tm_min,calendar.tm_sec);
		fprintf(fp,"<td>&emsp;%d 分ごとに巡回</td><tr><td>%d/%d スレッド</td>",targetBoard->interval/60,ret,maxThreadCount);
		if(targetBoard->lastVisited) {
			int increment = 0;
			for(const auto &thread : threadsAll) {
				if(thread->increment < 0) increment += thread->length;
				else increment += thread->increment;
			}
			float speed = 60.0f * increment / (now - targetBoard->lastVisited);
			const char *unit = "分";
			if(speed < 1.0f) {
				speed *= 60.0f;
				unit = "時";
			}
			fprintf(fp,"<td>%.1f レス/%s</td></table></div>\n",speed,unit);
		}
		else fprintf(fp,"<td>- レス/分</td></table></div>\n");
	}
	fprintf(fp,"<hr>\n<div class=\"menu\">\n");
	fprintf(fp,"<a target=\"_self\" href=\"javascript:location.reload();\">更新する</a>&ensp;<a href=\"%s\">本家へ</a>&ensp;<a target=\"_self\" href=\"javascript:void(0);\" id=\"option-button\">設定</a>&ensp;|",targetBoard->baseURL);
	for(const auto &board : boards) {
		if(board->interval == 0) continue;
		if(board == targetBoard) fprintf(fp,"&ensp;<b>%s</b>",board->name);
		else fprintf(fp,"&ensp;<a href=\"%s_%s.html\">%s</a>",board->server,board->board,board->name);
	}
	fprintf(fp,"</div>\n<div class=\"options\">横方向並び<input type=\"radio\" name=\"sortOrder\" value=\"0\" checked>新しい順\n");
	fprintf(fp,"<input type=\"radio\" name=\"sortOrder\" value=\"1\">多い順&emsp;<input type=\"checkbox\" id=\"autoreload\" %s>自動更新",targetBoard->interval?"":"disabled");
	fprintf(fp,"&emsp;<input type=\"checkbox\" id=\"preview\">プレビューを表示&emsp;<input type=\"checkbox\" id=\"showheat\">勢いを視覚化\n");
	fprintf(fp,"<form id=\"ngform\" target=\"_self\">NGワード&nbsp;<input type=\"text\" id=\"ngwords\">");
	fprintf(fp,"<button type=\"submit\">登録</button></form></div>\n");
	fprintf(fp,"<form id=\"searchform\" target=\"_self\">\n");
	fprintf(fp,"<input type=\"text\" id=\"searchwords\">");
	fprintf(fp,"<button type=\"submit\">検索</button></form>\n");
	fprintf(fp,"<div class=\"tabs\">\n");
	fprintf(fp,"<input id=\"tab-1\" type=\"radio\" name=\"tab-radio\" checked>");
	fprintf(fp,"<label class=\"tab-label\" for=\"tab-1\">検索結果</label>");
	fprintf(fp,"<input id=\"tab-2\" type=\"radio\" name=\"tab-radio\">");
	fprintf(fp,"<label class=\"tab-label\" for=\"tab-2\">勢いトップ</label>");
	if (!threadsHeat1H.empty() && std::get<0>(threadsHeat1H.front()->lengthHistory.back())) {
		fprintf(fp,"<input id=\"tab-7\" type=\"radio\" name=\"tab-radio\">");
		fprintf(fp,"<label class=\"tab-label\" for=\"tab-7\">勢いトップ(1h)</label>");
	}
	fprintf(fp,"<input id=\"tab-3\" type=\"radio\" name=\"tab-radio\">");
	fprintf(fp,"<label class=\"tab-label\" for=\"tab-3\">レス数トップ</label>");
	fprintf(fp,"<input id=\"tab-4\" type=\"radio\" name=\"tab-radio\">");
	fprintf(fp,"<label class=\"tab-label\" for=\"tab-4\">隔離済</label>");
	fprintf(fp,"<input id=\"tab-5\" type=\"radio\" name=\"tab-radio\">");
	fprintf(fp,"<label class=\"tab-label\" for=\"tab-5\">削除済</label>");
	if (targetBoard->detectGarbage && !threadsGarbage.empty()) {
		fprintf(fp,"<input id=\"tab-6\" type=\"radio\" name=\"tab-radio\">");
		fprintf(fp,"<label class=\"tab-label\" for=\"tab-6\">ゴミ箱</label>");
	}
	fprintf(fp,"\n<div class=\"tab-content tab-1-content\">\n");
	fprintf(fp,"<table id=\"favorites\" class=\"nom\" cellspacing=\"0\"></table>\n");
	fprintf(fp,"</div><div class=\"tab-content tab-2-content\">\n");
	emitTableHTML(fp,targetBoard,threadsHeat,false,false,2);
	fprintf(fp,"</div><div class=\"tab-content tab-3-content\">\n");
	emitTableHTML(fp,targetBoard,threadsPop,false,false,2);
	fprintf(fp,"</div><div class=\"tab-content tab-4-content\">\n");
	emitTableHTML(fp,targetBoard,threadsIsolated,false,false,1);
	fprintf(fp,"</div><div class=\"tab-content tab-5-content\">\n");
	emitTableHTML(fp,targetBoard,threadsDeleted,false,false,0);
	if (targetBoard->detectGarbage && !threadsGarbage.empty()) {
		fprintf(fp,"</div><div class=\"tab-content tab-6-content\">\n");
		emitTableHTML(fp,targetBoard,threadsGarbage,false,false,0);
	}
	if (!threadsHeat1H.empty()) {
		fprintf(fp,"</div><div class=\"tab-content tab-7-content\">\n");
		emitTableHTML(fp,targetBoard,threadsHeat1H,false,false,2);
	}
	fprintf(fp,"</div></div>\n");
	emitTableHTML(fp,targetBoard,threads,false,true,1);
	fprintf(fp,"<br><div align=\"center\">↓レスの少ないスレ</div>\n");
	emitTableHTML(fp,targetBoard,threadsSlow,true,true,1);
	fprintf(fp,"<hr><div align=\"right\"><em>Generated by <a href=https://gitlab.com/toshiaki774/FutabaCatalogGen>FutabaCatalogGen</a></em></div>\n");
	fprintf(fp,"<script type=\"application/json\" id=\"json-data\">\n{");
	for(const auto &thread : threadsAll) {
		if(thread != *threadsAll.begin()) fprintf(fp,",");
		fprintf(fp,"\"t%04d\":\"%s\"\n",thread->tag,thread->normalizedComment.c_str());
	}
	for(const auto &thread : threadsIsolated) {
		if(threadsAll.size() != 0 || thread != *threadsIsolated.begin()) fprintf(fp,",");
		fprintf(fp,"\"t%04d\":\"%s\"\n",thread->tag,thread->normalizedComment.c_str());
	}
	fprintf(fp,"}</script>\n");
	fprintf(fp,"<script type=\"text/javascript\">var threadsPerRow = %d;\nvar nextUpdate = %ld000;</script>\n",threadsPerRow,targetBoard->interval?time(NULL)+targetBoard->interval+10:0);
	fprintf(fp,"<!--[if lte IE8]>\n");
	fprintf(fp,"<script src=\"https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js\"></script>\n");
	fprintf(fp,"<script src=\"https://cdn.jsdelivr.net/npm/jquery.cookie@1.4.1/jquery.cookie.min.js\"></script>\n");
	fprintf(fp,"<script src=\"https://cdn.jsdelivr.net/npm/object-fit-images@3.2.4/dist/ofi.min.js\"></script>\n");
	fprintf(fp,"<script src=\"resources/script.js%s\"></script>\n", scriptQueryString.c_str());
	fprintf(fp,"<![endif]-->\n");
	fprintf(fp,"<!--[if gte IE 9]><!-->\n");
	fprintf(fp, R"(<script type="text/javascript">
	(function() {
		function loadScript(src, onload) {
			var script = document.createElement("script");
			script.src = src;
			if (onload) script.onload = onload;
			document.body.appendChild(script);
		}
		if ("noModule" in HTMLScriptElement.prototype) {
			loadScript("resources/)" MODERN_SCRIPT_FILENAME R"(%s");
		}
		else {
			loadScript("https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js", function() {
				loadScript("https://cdn.jsdelivr.net/npm/jquery.cookie@1.4.1/jquery.cookie.min.js", function() {
					loadScript("resources/script.js%s");
				});
				loadScript("https://cdn.jsdelivr.net/npm/object-fit-images@3.2.4/dist/ofi.min.js");
			});
		}
	}());)" "\n</script>\n", modernScriptQueryString.c_str(), scriptQueryString.c_str());
	fprintf(fp,"<!--<![endif]-->\n");
	fprintf(fp,"</body></html>");
	fclose(fp);
	return 0;
}

static int parseBoardsTxt(const char *filename)
{
	static long lastChecked;
	time_t now = time(NULL);
	if(lastChecked) {
		struct stat s;
		if(now - lastChecked < 60) return 0;
		if(stat(filename, &s) != 0) return 0;
		if(s.st_mtime <= lastChecked) {
			lastChecked = now;
			return 0;
		}
		log_printf("Detected modification in %s\n",filename);
	}
	FILE *fp = fopen(filename,"r");
	if(!fp) {
		fprintf(stderr,"cannot open %s\n",filename);
		return -1;
	}
	std::unordered_map<std::string, PFCGBoard> previousBoards;
	for(auto &&board : boards) {
		previousBoards[board->identifier()] = std::move(board);
	}
	boards.clear();
	char buf[1024];
	int line = 1;
	while(fgets(buf,1024,fp) != NULL) {
		PFCGBoard tmp;
		char *ptr, *start, *server, *board, *name;
		bool secure = false;
		if(buf[0] == '\n' || buf[0] == '#' ||buf[0] == 0) goto end;
		if(strncmp(buf,"http://",7) && strncmp(buf,"https://",8)) {
			goto error;
		}
		if(buf[5] == ':') secure = true;
		ptr = 3 + strstr(buf,"://");
		start = ptr;
		while(*ptr != '.' && *ptr != '/' && *ptr != 0) ptr++;
		if(*ptr != '.') goto error;
		*ptr++ = 0;
		server = start;
		while(*ptr != '/' && *ptr != 0) ptr++;
		if(*ptr != '/') goto error;
		start = ++ptr;
		while(*ptr != '/' && *ptr != ',' && *ptr != 0) ptr++;
		if(*ptr == 0) goto error;
		if(*ptr == ',') {
			*ptr = 0;
			board = start;
		} else {
			*ptr++ = 0;
			board = start;
			while(*ptr != ',' && *ptr != 0) ptr++;
			if(*ptr == 0) goto error;
		}
		start = ++ptr;
		while(*ptr != ',' && *ptr != 0) ptr++;
		if(*ptr == 0) goto error;
		*ptr++ = 0;
		name = start;
		if(!previousBoards.empty()) {
			std::string key = server;
			key += "/";
			key += board;
			auto it = previousBoards.find(key);
			if(it != previousBoards.end()) {
				tmp = std::move(it->second);
				previousBoards.erase(it);
			}
		}
		if(!tmp) tmp.reset(new FCGBoard(server,board,name,secure));
		tmp->lengthThreshold = strtoul(ptr,&ptr,10);
		while(*ptr != ',' && *ptr != 0) ptr++;
		if(*ptr++ == 0) goto error;
		tmp->interval = strtoul(ptr,NULL,10);
		if(tmp->interval) log_printf("Get %s (%s) every %d seconds\n",tmp->baseURL,tmp->name,tmp->interval);
		while(*ptr != ',' && *ptr != 0) ptr++;
		if(*ptr == ',') {
			tmp->detectGarbage = strtoul(ptr+1,NULL,10);
		}
		boards.push_back(std::move(tmp));
		goto end;
	error:
		fprintf(stderr,"%s: parse error at line %d\n",filename,line);
	end:
		line++;
	}
	fclose(fp);
	lastChecked = now;
	return boards.size();
}

static void parseNGTxt(const char *filename)
{
	static long lastChecked;
	time_t now = time(NULL);
	struct stat s;
	if(now - lastChecked < 60) return;
	if(stat(filename, &s) != 0) return;
	if(s.st_mtime > lastChecked) {
		FILE *fp = fopen(filename, "r");
		char buf[1024];
		ngWords.clear();
		while(fgets(buf,1024,fp)) {
			char *ptr = buf, *end;
			if(*ptr == '#') continue;
			while(isspace((unsigned char)*ptr)) ptr++;
			if(*ptr == 0) continue;
			end = ptr + strlen(ptr) - 1;
			while(end > ptr && isspace((unsigned char)*end)) end--;
			end[1] = 0;
			int len = end - ptr + 1;
			uint32_t *utf32 = new uint32_t[len+1];
			char *normalized = new char[len*2+1];
			size_t words = convertUTF8ToUTF32(utf32, ptr, len);
			normalizeUTF32toUTF8(normalized, utf32, words);
			ngWords.push_back(normalized);
			delete [] utf32;
			delete [] normalized;
		}
		fclose(fp);
		log_printf("Updated NG word list (%d words)\n",ngWords.size());
	}
	lastChecked = now;
}

static void signalHandler(int sig)
{
	quit = 1;
}

static void getScriptQueryString(const std::string &basePath)
{
	static time_t lastChecked;
	time_t now = time(NULL);
	struct stat s;
	if (now - lastChecked < 60) return;
	std::string path = basePath + "/resources/script.js";
	char buf[32];
	if (stat(path.c_str(), &s) == 0) {
		snprintf(buf, 32, "?%ld", s.st_mtime);
		scriptQueryString = std::string(buf);
	}
	else scriptQueryString = "?0";
	path = basePath + "/resources/" MODERN_SCRIPT_FILENAME;
	if (stat(path.c_str(), &s) == 0) {
		snprintf(buf, 32, "?%ld", s.st_mtime);
		modernScriptQueryString = std::string(buf);
	}
	else modernScriptQueryString = "?0";
	path = basePath + "/resources/style.css";
	if (stat(path.c_str(), &s) == 0) {
		snprintf(buf, 32, "?%ld", s.st_mtime);
		cssQueryString = std::string(buf);
	}
	else cssQueryString = "?0";
	lastChecked = now;
}

static void saveStateToPath(const std::string &path)
{
#if (__cplusplus < 202002L) || !defined(USE_GLAZE)
	std::ofstream ofs(path, std::ios::binary);
	if(ofs.is_open()) {
		jsoncons::encode_json(threadsHistory, ofs);
		ofs.close();
	}
#else
	glz::write_file_json(threadsHistory, path, std::string{});
#endif
}

static void loadStateFromPath(const std::string &path)
{
#if (__cplusplus < 202002L) || !defined(USE_GLAZE)
	std::ifstream ifs(path, std::ios::binary);
	if(ifs.is_open()) {
		try {
			threadsHistory = jsoncons::decode_json<FCGThreadsDictionary>(ifs);
		} catch(...) {
			return;
		}
		ifs.close();
	}
#else
	FCGThreadsDictionary tmp;
	auto err = glz::read_file_json<glz::opts{.error_on_unknown_keys = false}>(tmp, path, std::string{});
	if (!err) threadsHistory = std::move(tmp);
	else {
		return;
	}
#endif
	for(auto it = threadsHistory.begin(); it != threadsHistory.end();) {
		const std::string &key = it->first;
		bool shouldErase = true;
		for(const auto &board : boards) {
			if(key == board->identifier()) {
				shouldErase = false;
				break;
			}
		}
		if(shouldErase) {
			it = threadsHistory.erase(it);
		} else {
			time_t now = time(NULL);
			for (auto &&thread : std::get<0>(it->second)) {
				thread->flags &= ~(FCGThread::FLAG_GARBAGE | FCGThread::FLAG_NOT_GARBAGE);
				for (auto it = thread->lengthHistory.begin(); it != thread->lengthHistory.end(); ++it) {
					if (now - std::get<0>(*it) > 60*(60+30)) {
						thread->lengthHistory.erase(it, thread->lengthHistory.end());
						break;
					}
				}
			}
			log_printf("Recovered thread data for %s\n",key.c_str());
			it++;
		}
	}
}

int main(int argc, char *argv[])
{
	int opt;
	bool saveState = false;
#ifdef _WIN32
	SetConsoleOutputCP(CP_UTF8);
#endif
	while((opt = getopt_long(argc, argv, "o:b:n:m:ks",NULL,NULL)) != -1) {
		switch(opt) {
			case 'o':
				htmlPath = optarg;
				break;
			case 'b':
				boardsTxtPath = optarg;
				break;
			case 'n':
				NGTxtPath = optarg;
				break;
			case 'm':
				maxThreadCount = atoi(optarg);
				break;
			case 'k':
				checkCertificate = false;
				break;
			case 's':
				saveState = true;
				break;
			default:
				fprintf(stderr,"Usage: %s [-o /path/to/output/directory] [-b /path/to/boards.txt] [-n /path/to/ngword.txt]\n",argv[0]);
				return 0;
		}
	}
	struct stat s;
	if(stat(htmlPath, &s) != 0) {
		perror(htmlPath);
		return 0;
	}
	if(!S_ISDIR(s.st_mode)) {
		fprintf(stderr,"%s is not a directory\n",htmlPath);
		return 0;
	}
	parseNGTxt(NGTxtPath);
	
	if(maxThreadCount % 50 != 0) {
		maxThreadCount = (maxThreadCount / 50 + 1) * 50;
		log_printf("Note: max thread count is set to %d\n",maxThreadCount);
	}
	
	curl_global_init(CURL_GLOBAL_DEFAULT);
	if(argc-optind < 2) {
		if(parseBoardsTxt(boardsTxtPath) <= 0) {
			fprintf(stderr,"no board found, exiting\n");
			return 0;
		}
		std::string jsonLogPath(htmlPath);
		jsonLogPath += "/fcg_saved_state.json";
		if(saveState) {
			loadStateFromPath(jsonLogPath);
			signal(SIGINT, signalHandler);
		}
		while(!quit) {
			time_t now = time(NULL);
			getScriptQueryString(htmlPath);
			for(const auto &board : boards) {
				if(board->interval == 0) continue;
				if(now - board->lastVisited < board->interval) continue;
				if(generateCatalog(board) == 0) board->lastVisited = now;
			}
			sleep(5);
			parseBoardsTxt(boardsTxtPath);
			parseNGTxt(NGTxtPath);
		}
		if(saveState) saveStateToPath(jsonLogPath);
	}
	else {
		PFCGBoard board(new FCGBoard(argv[optind],argv[optind+1],"Futaba",false));
		generateCatalog(board);
	}
	
	return 0;
}
