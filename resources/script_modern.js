(function () {
let threadData = null;
let threadKeys = null;
let useAjaxReloadingOnServer = true;

class PreviewPopup {
    constructor(clientX, clientY, source, manager) {
        this.source = source;
        this.manager = manager;
        this.shown = false;
        this.clientX = clientX;
        this.clientY = clientY;
        this.doc = source.ownerDocument;
        this.win = this.doc.defaultView;
    }
    create() {
        const doc = this.doc;
        const win = this.win;
        if (threadData == null) {
            threadData = JSON.parse(doc.getElementById("json-data").text);
        }
        const popup = this.popup = doc.createElement("div");
        popup.className = "tooltip";
        const threadLink = this.source.querySelector("a").href;
        const match = threadLink.match(/(https?:\/\/\w+\.2chan\.net\/)(\w+)\/res\/(\d+)\.htm/);
        const imgHolder = doc.createElement("a");
        imgHolder.href = threadLink;
        const sourceImg = this.source.querySelector("img");
        const img = doc.createElement("img");
        img.className = "tooltip_thumb";
        if (sourceImg) {
            img.src = sourceImg.src?.replace("/cat/", "/thumb/");
        }
        imgHolder.appendChild(img);
        const text = doc.createElement("div");
        const parentColumn = this.source.closest("td");
        text.className = "tooltip_text";
        text.textContent = threadData[parentColumn.id || parentColumn.getAttribute("id-orig")];
        const note = doc.createElement("div");
        note.className = "tooltip_note";
        note.appendChild(this.source.querySelector(".cow").cloneNode(true));
        if (match) {
            note.insertAdjacentHTML("beforeend", `<a class="delbutton" href="${match[1]}del.php?b=${match[2]}&d=${match[3]}"><i class="fas fa-trash"></i></a>`);
        }
        popup.appendChild(imgHolder);
        popup.appendChild(text);
        popup.appendChild(note);
        let left = win.scrollX + this.clientX;
        if (this.clientX + 328 > this.viewportWidth) {
            left = win.scrollX + this.viewportWidth - 328;
        }
        let top = win.scrollY + this.clientY;
        if (this.clientY + 174 > this.viewportHeight) {
            top = win.scrollY + this.viewportHeight - 174;
        }
        Object.assign(popup.style, {
            top: `${top}px`,
            left: `${left}px`,
            "font-size": "13px",
            display: "block",
            opacity: 0,
            transition: "opacity 0.2s ease",
            zIndex: "1000",
        });
        popup.addEventListener("mouseenter", this);
        popup.addEventListener("mouseleave", this);
    }
    show() {
        const timeout = 700;
        if (this.shown || this.showCanceller) return;
        return new Promise((resolve, reject) => {
            this.showCanceller = reject;
            this.win.setTimeout(() => {
                resolve();
            }, timeout);
            this.source.addEventListener("mousemove", this);
        }).then(() => {
            this.source.removeEventListener("mousemove", this);
            this.create();
            this.shown = true;
            this.showCanceller = null;
            this.doc.body.appendChild(this.popup);
            this.win.setTimeout(() => {
                this.popup.style.opacity = 1.0;
            }, 0);
        }).catch(() => {
            this.source.removeEventListener("mousemove", this);
            this.showCanceller = null;
            this.manager.popupDisappeared(this.source);
        });
    }
    cancelShowing() {
        if (this.showCanceller) this.showCanceller();
    }
    hide() {
        const timeout = 100;
        if (!this.shown || this.focused || this.hideCanceller) return;
        return new Promise((resolve, reject) => {
            this.hideCanceller = reject;
            this.win.setTimeout(() => {
                resolve();
            }, timeout);
        }).then(() => {
            this.hideCanceller = null;
            this.destruct();
        }).catch(() => {
            this.hideCanceller = null;
        });
    }
    cancelHiding() {
        if (this.hideCanceller) this.hideCanceller();
    }
    destruct() {
        this.shown = false;
        this.manager.popupDisappeared(this.source);
        let timer = this.win.setTimeout(() => {
            this.popup.remove();
        }, 500);
        this.popup.addEventListener("transitionend", ({target}) => {
            this.win.clearTimeout(timer);
            target.remove();
        }, {once:true});
        this.popup.style.opacity = 0;
    }
    handleEvent(event) {
        switch(event.type) {
            case "mouseenter":
                this.focused = true;
                this.cancelHiding();
                break;
            case "mouseleave":
                this.focused = false;
                this.hide();
                break;
            case "mousemove":
                this.clientX = event.clientX;
                this.clientY = event.clientY;
                break;
        }
    }
    get viewportWidth() {
        return Math.min(this.win.innerWidth, this.doc.documentElement.clientWidth);
    }
    get viewportHeight() {
        return Math.min(this.win.innerHeight, this.doc.documentElement.clientHeight);
    }
}

class PopupManager {
    constructor(win) {
        this.enabled = false;
        this.sourceElements = new WeakMap();
        this.activePopups = new WeakMap();
    }
    attachToElement(element, kind = 0) {
        if (!this.enabled) return;
        if (this.sourceElements.has(element)) return;
        if (element.closest(".tab-content")?.classList.contains("tab-5-content")) return;
        if (element.closest(".tab-content")?.classList.contains("tab-6-content")) return;
        this.sourceElements.set(element, kind);
        element.addEventListener("mouseenter", this);
        element.addEventListener("mouseleave", this);
    }
    popupDisappeared(source) {
        this.activePopups.delete(source);
    }
    handleEvent(event) {
        let popup;
        switch(event.type) {
            case "mouseenter":
                popup = this.activePopups.get(event.currentTarget);
                if (popup) {
                    popup.cancelHiding();
                    return;
                }
                if (!this.enabled || event.metaKey || event.ctrlKey) return;
                popup = new PreviewPopup(event.clientX, event.clientY, event.currentTarget, this);
                this.activePopups.set(event.currentTarget, popup);
                popup.show();
                break;
            case "mouseleave":
                popup = this.activePopups.get(event.currentTarget);
                if (popup) {
                    popup.hide();
                    popup.cancelShowing();
                }
                break;
        }
    }
};

class JunkThreadsFilter {
    constructor(delegate) {
        this.fcg = delegate;
        this.window = delegate.window;
        this.document = delegate.document;
        this.migrateConf();
        let confKey = this.window.location.pathname.split("/").pop().split(".").shift();
        let conf = this.fcg.getValueFromStorage("fcg_hideJunkThreadsConf2");
        let rules = [];
        if (conf != null) {
            conf = JSON.parse(conf);
            if (typeof conf !== "object") rules = [];
            else {
                rules = conf[confKey] || [];
            }
        }
        if (!rules.length) {
            rules.push({
                enabled: 0,
                thresholdCount: 0,
                thresholdDuration: 10
            });
        } else {
            for (let rule of rules) {
                rule.thresholdCount = parseInt(rule.thresholdCount) || 0;
                rule.thresholdDuration = parseInt(rule.thresholdDuration) || 0;
            }
        }
        this.confKey = confKey;
        this.rules = rules;
    }
    migrateConf() {
        let conf = this.fcg.getValueFromStorage("fcg_hideJunkThreadsConf");
        if (!conf) return;
        let newConf = {};
        conf = JSON.parse(conf);
        if (typeof conf === "object") {
            for (let key in conf) {
                let {hideJunk, thresholdCount, thresholdDuration} = conf[key];
                newConf[key] = [{
                    enabled: hideJunk,
                    thresholdCount,
                    thresholdDuration
                }];
            }
        }
        this.fcg.setValueToStorage("fcg_hideJunkThreadsConf2", JSON.stringify(newConf));
        this.window.localStorage.removeItem("fcg_hideJunkThreadsConf");
    }
    putOptions() {
        let optionsArea = this.document.querySelector(".junk_options");
        if (!optionsArea) {
            this.document.querySelector(".options").insertAdjacentHTML("beforeend", `
                <div class="junk_options">以下のルールでスレを非表示にする: 
                    <button type="submit" id="apply_rules">適用</button>
                </div>
            `);
            optionsArea = this.document.querySelector(".junk_options");
            let count = 0;
            for (let rule of this.rules) {
                let {enabled, thresholdCount, thresholdDuration, onlyForNoImage} = rule;
                optionsArea.insertAdjacentHTML("beforeend", `
                    <div class="junk_rule">
                        <input type="checkbox" class="rule_enabled" ${enabled ? "checked" : ""}>
                        <input type="text" value="${thresholdDuration}" class="rule_threshold_min" inputmode="numeric">分経過して
                        <input type="text" value="${thresholdCount}" class="rule_threshold_count" inputmode="numeric">レス以下のスレは表示しない
                        <input type="checkbox" class="only_for_noimage" ${onlyForNoImage ? "checked" : ""}>画像なしのみ
                        <button type="submit" class="remove_rule">&#8722;</button>
                        ${count === this.rules.length - 1 ? `<button type="submit" id="add_rule">&#43;</button>` : ""}
                    </div>`);
                count++;
            }
            optionsArea.querySelector("#apply_rules").addEventListener("click", () => {
                let newConf = this.fcg.getValueFromStorage("fcg_hideJunkThreadsConf2");
                if (newConf == null) newConf = {};
                else {
                    newConf = JSON.parse(newConf);
                    if (!newConf) newConf = {};
                }
                let newRules = [];
                for (let ruleArea of optionsArea.querySelectorAll(".junk_rule")) {
                    newRules.push({
                        enabled: ruleArea.querySelector(".rule_enabled").checked ? 1 : 0,
                        thresholdCount: ruleArea.querySelector(".rule_threshold_count").value,
                        thresholdDuration: ruleArea.querySelector(".rule_threshold_min").value,
                        onlyForNoImage: ruleArea.querySelector(".only_for_noimage").checked ? 1 : 0,
                    });
                }
                newConf[this.confKey] = newRules;
                this.fcg.setValueToStorage("fcg_hideJunkThreadsConf2", JSON.stringify(newConf));
                this.window.location.reload();
            });
            optionsArea.querySelector("#add_rule").addEventListener("click", event => {
                optionsArea.insertAdjacentHTML("beforeend", `
                    <div class="junk_rule">
                        <input type="checkbox" class="rule_enabled">
                        <input type="text" value="10" class="rule_threshold_min" inputmode="numeric">分経過して
                        <input type="text" value="0" class="rule_threshold_count" inputmode="numeric">レス以下のスレは表示しない
                        <input type="checkbox" class="only_for_noimage">画像なしのみ
                        <button type="submit" class="remove_rule">&#8722;</button>
                    </div>`);
                optionsArea.lastElementChild.appendChild(event.currentTarget);
                this.rules.push({
                    enabled: 0,
                    thresholdCount: "0",
                    thresholdDuration: "10",
                    onlyForNoImage: 0,
                });
            });
            optionsArea.addEventListener("click", event => {
                if (event.target.className !== "remove_rule") return;
                if (this.rules.length <= 1) return;
                let idx = 0;
                let targetArea = event.target.closest(".junk_rule");
                let addButton = optionsArea.querySelector("#add_rule");
                for (let ruleArea of optionsArea.querySelectorAll(".junk_rule")) {
                    if (targetArea === ruleArea) {
                        this.rules.splice(idx, 1);
                        ruleArea.remove();
                        break;
                    }
                    idx++;
                }
                optionsArea.lastElementChild.appendChild(addButton);
            });
        }
    }
    hasValidRule() {
        for (let rule of this.rules) {
            if (rule.enabled) return true;
        }
        return false;
    }
    apply() {
        try {
            if (!this.hasValidRule()) return;
            if (threadData == null) {
                threadData = JSON.parse(this.window.document.getElementById("json-data").text);
            }
            let now = Date.now();
            this.document.querySelectorAll(".sortable .thread").forEach(threadTd => {
                let count = threadTd.querySelector(".cow")?.firstChild?.nodeValue;
                if (count == null) return;
                let imgTag = threadTd.querySelector("img");
                let birth = imgTag?.src?.split("/").pop() || imgTag?.getAttribute("data-original")?.split("/").pop();
                if (imgTag == null) {
                    birth = parseInt(threadTd.querySelector(".cat span")?.getAttribute("birth")) * 1000;
                }
                if (birth == null) return;
                let doFilter = false;
                for (let rule of this.rules) {
                    if (!rule.enabled) continue;
                    if (imgTag && rule.onlyForNoImage) continue;
                    if (parseInt(count) > rule.thresholdCount) continue;
                    if (parseInt(birth) > now - rule.thresholdDuration*60*1000) continue;
                    doFilter = true;
                    break;
                }
                if (!doFilter) return;
                delete(threadData[threadTd.id]);
                if (!threadTd.nextElementSibling && !threadTd.previousElementSibling?.classList.contains("thread")) {
                    threadTd.parentNode.remove();
                }
                else threadTd.remove();
                this.fcg.deletedThreads++;
            });
        } catch (e) {
            console.error(e);
        }
    }
};

function loadScript(source, doc) {
    return new Promise((resolve, reject) => {
        const script = doc.createElement("script");
        script.src = source;
        script.onload = () => {
            resolve();
        };
        script.onerror = () => {
            reject(new Error(`Failed to load ${script.src}`));
        };
        doc.body.appendChild(script);
    });
}

class FCG {
    constructor(win) {
        this.window = win;
        this.document = win.document;
        if (win.localStorage.getItem("fcg_legacy") == 1) {
            this.initLegacy();
            return;
        }
        this.currentTabContentHeight = 0;
        this.touchStartY = 0;
        this.touchMoveY = 0;
        this.originalScale = 0;
        this.reloadTimer = null;
        this.mobileReloadTimer = null;
        this.popupManager = new PopupManager(win);
        this.deletedThreads = 0;
        this.lazyLoad = new Promise((resolve, reject) => {
            const lazyLoadSrc = "IntersectionObserver" in win ?
                "https://cdn.jsdelivr.net/npm/vanilla-lazyload@17/dist/lazyload.min.js" :
                "https://cdn.jsdelivr.net/npm/vanilla-lazyload@8/dist/lazyload.min.js";
            loadScript(lazyLoadSrc, win.document)
            .then(() => resolve(win.LazyLoad))
            .catch(err => reject(err));
        });
        new Promise(resolve => {
            if (win.document.readyState === "loading") {
                win.addEventListener("DOMContentLoaded", (event) => {
                    resolve();
                }, {once: true});
            } else {
                resolve();
            }
        }).then(() => {
            this.init();
        });
    }
    init() {
        //const start_ms = new Date().getTime();
        const win = this.window;
        const doc = this.document;
        const words = this.getValueFromStorage("fcg_searchwords");
        const ngwords = this.getValueFromStorage("fcg_ngwords");
        const sortOrder = this.getValueFromStorage("fcg_sortorder");
        const autoReload = this.getValueFromStorage("fcg_autoreload");
        const preview = this.getValueFromStorage("fcg_preview");
        const showHeat = this.getValueFromStorage("fcg_showheat");
        this.isDarkMode = this.getValueFromStorage("fcg_darkmode") == 1;
        this.omitIsolated = this.getValueFromStorage("fcg_omitIsolated") == 1;
        try {
            let junkFilter = new JunkThreadsFilter(this);
            junkFilter.putOptions();
            junkFilter.apply();
        } catch (e) {console.error(e)};
        if (ngwords != null) {
            doc.getElementById("ngwords").value = ngwords;
            this.doFilterWithBlocklist(ngwords);
        }
        else doc.getElementById("ngwords").value = "";
        this.fixupTable();
        if (words != null) {
            doc.getElementById("searchwords").value = words;
            this.doSearch(words);
        }
        else doc.getElementById("searchwords").value = "";
        if (sortOrder != null) {
            if (parseInt(sortOrder) === 1) {
                doc.querySelector("input[name=\"sortOrder\"]").checked = false;
                doc.querySelector("input[name=\"sortOrder\"][value=\"1\"]").checked = true;
            }
        }
        if (autoReload != null) {
            if (parseInt(autoReload) === 1 && win.nextUpdate) {
                doc.getElementById("autoreload").checked = true;
            }
        }
        if (preview != null) {
            if (parseInt(preview) === 1) {
                doc.getElementById("preview").checked = true;
                this.popupManager.enabled = true;
            }
        }
        if (showHeat != null) {
            if (parseInt(showHeat) === 1) {
                doc.getElementById("showheat").checked = true;
            }
        }
        if (useAjaxReloadingOnServer && win.location.protocol.startsWith("http")) {
            win.nextUpdate = 0;
            const tmp = doc.getElementById("autoreload");
            tmp.parentNode.removeChild(tmp.nextSibling);
            tmp.checked = false;
            tmp.disabled = true;
            tmp.style.display = "none";
            const reloadButton = doc.querySelector(".menu a");
            reloadButton.href = "javascript:void(0);";
            reloadButton.addEventListener("click", event => {
                reloadButton.style.display = "none";
                reloadButton.insertAdjacentHTML("afterend", '<span id="ajaxmsg">更新中...</span>');
                const controller = new AbortController();
                const timeout = win.setTimeout(() => { controller.abort() }, 10000);
                win.fetch(win.location.href, {
                    method: "HEAD",
                    cache: "no-cache",
                    signal: controller.signal,
                }).then(resp => {
                    win.clearTimeout(timeout);
                    if (!resp.ok) throw new Error(resp.statusText);
                    const dateCurrent = new Date(doc.lastModified);
                    const dateNew = new Date(resp.headers.get("Last-Modified"));
                    if(dateNew > dateCurrent) win.location.reload();
                    else {
                        const msgBox = doc.getElementById("ajaxmsg");
                        msgBox.textContent = "更新なし";
                        win.setTimeout(() => {
                            msgBox.previousElementSibling.style.display = null;
                            msgBox.remove();
                        }, 1500);
                    }
                }).catch(err => {
                    win.clearTimeout(timeout);
                    const msgBox = doc.getElementById("ajaxmsg");
                    msgBox.textContent = `通信エラー (${err.message})`;
                    win.setTimeout(() => {
                        msgBox.previousElementSibling.style.display = null;
                        msgBox.remove();
                    }, 1500);
                });
            });
        }
        doc.getElementById("searchform").addEventListener("submit", event => {
            event.preventDefault();
            const text = doc.getElementById("searchwords").value;
            this.setValueToStorage("fcg_searchwords", text);
            this.doSearch(text);
            doc.getElementById("tab-1").checked = true;
            this.updateCurrentTabContentHeight();
        });
        doc.getElementById("ngform").addEventListener("submit", event => {
            event.preventDefault();
            const text = doc.getElementById("ngwords").value;
            doc.querySelector(".warn")?.remove();
            if (text.charAt(0) === "(") {
                try {
                    const regex = new RegExp(text,'i');
                } catch (err) {
                    doc.getElementById("ngform").insertAdjacentHTML("afterend", '<div class="warn">正規表現に誤りがあります</div>');
                    return;
                }
            }
            this.setValueToStorage("fcg_ngwords", text);
            win.location.reload();
        });
        doc.querySelectorAll("input[name=\"sortOrder\"]").forEach(elem => elem.addEventListener("change", event => {
            if (event.target.value === "1") {
                this.setValueToStorage("fcg_sortorder", 1);
                for (let row of doc.querySelectorAll(".sortable tr")) {
                    const sorted = Array.from(row.querySelectorAll(".thread")).sort((a, b) => {
                        const lengthA = parseInt(a.getAttribute("data-length"));
                        const lengthB = parseInt(b.getAttribute("data-length"));
                        return lengthB - lengthA;
                    });
                    for (let column of sorted) {
                        row.appendChild(column);
                    }
                }
            } else {
                this.setValueToStorage("fcg_sortorder", 0);
                for (let row of doc.querySelectorAll(".sortable tr")) {
                    const sorted = Array.from(row.querySelectorAll(".thread")).sort((a, b) => {
                        const hrefA = a.querySelector("a")?.href;
                        const hrefB = b.querySelector("a")?.href;
                        if (hrefB > hrefA) return 1;
                        if (hrefA > hrefB) return -1;
                        return 0;
                    });
                    for (let column of sorted) {
                        row.appendChild(column);
                    }
                }
            }
        }));
        doc.getElementById("autoreload").addEventListener("change", event => {
            if (!win.nextUpdate) return;
            if (event.target.checked) {
                this.setValueToStorage("fcg_autoreload", 1);
                if (this.reloadTimer == null) {
                    const timeToReload = win.nextUpdate - (new Date()).getTime();
                    this.reloadTimer = win.setTimeout(() => {
                        win.location.reload();
                    }, timeToReload > 0 ? timeToReload : 60000);
                }
            } else {
                this.setValueToStorage("fcg_autoreload", 0);
                if (this.reloadTimer != null) {
                    win.clearTimeout(this.reloadTimer);
                    this.reloadTimer = null;
                }
            }
        });
        doc.getElementById("preview").addEventListener("change", event => {
            if (event.target.checked) {
                this.setValueToStorage("fcg_preview", 1);
                this.popupManager.enabled = true;
                doc.querySelectorAll(".res1").forEach(elem => this.popupManager.attachToElement(elem));
            } else {
                this.setValueToStorage("fcg_preview", 0);
                this.popupManager.enabled = false;
            }
        });
        doc.getElementById("showheat").addEventListener("change", event => {
            if (event.target.checked) {
                this.setValueToStorage("fcg_showheat", 1);
                for (let element of doc.querySelectorAll("td.thread[heatbg]")) {
                    const color = element.getAttribute("heatbg");
                    if (this.isDarkMode) {
                        const heat = 1.2*(255 - parseInt(color.slice(3, 5), 16));
                        element.style.backgroundColor = `rgba(${64+heat},64,64,1.0)`;
                    }
                    else element.style.backgroundColor = color;
                }
            } else {
                this.setValueToStorage("fcg_showheat", 0);
                for (let element of doc.querySelectorAll("td.thread[heatbg]")) {
                    element.style.backgroundColor = null;
                }
            }
        });
        doc.querySelectorAll("input[name=\"tab-radio\"]").forEach(elem => elem.addEventListener("change", event => {
            const contentClass = event.target.id + "-content";
            const content = doc.querySelector(`.${contentClass}`);
            const contentTable = content.querySelector("table");
            const {paddingTop, paddingBottom} = win.getComputedStyle(content);
            const newTabContentHeight = content.clientHeight - parseFloat(paddingTop);
            let duration = 200;
            if (Math.abs(newTabContentHeight - this.currentTabContentHeight) < 10) {
                if (newTabContentHeight === this.currentTabContentHeight) duration = 0;
                else duration = 50;
            }
            if (duration) {
                contentTable.style.display = "none";
                content.style.paddingBottom = `${this.currentTabContentHeight}px`;
                content.addEventListener("transitionend", () => {
                    content.style.transition = null;
                    content.style.paddingBottom = null;
                    contentTable.style.display = "table";
                    for (let image of content.querySelectorAll("img[src-original]")) {
                        image.src = image.getAttribute("src-original");
                        image.removeAttribute("src-original");
                    }
                }, {once: true});
                win.setTimeout(() => {
                    content.style.transition = `padding-bottom ${duration}ms`;
                    content.style.paddingBottom = `${newTabContentHeight}px`;
                }, 20);
            }
            this.currentTabContentHeight = newTabContentHeight;
        }));
        doc.getElementById("option-button").addEventListener("click", event => {
            const options = doc.querySelector(".options");
            const {display} = win.getComputedStyle(options);
            let folding = true;
            let height;
            if (display === "none") {
                folding = false;
            }
            options.addEventListener("transitionend", () => {
                if (folding) options.style.display = null;
                options.style.transition = null;
                options.style.overflow = null;
                options.style.maxHeight = null;
                options.style.marginBottom = null;
            }, {once: true});
            options.style.overflow = "hidden";
            if (folding) {
                options.style.maxHeight = `${options.clientHeight}px`;
            }
            else {
                options.style.display = "block";
                height = options.clientHeight;
                options.style.maxHeight = "0px";
                options.style.marginBottom = "0px";
            }
            win.setTimeout(() => {
                options.style.transition = "max-height 200ms, margin-bottom 200ms";
                if (folding) {
                    options.style.maxHeight = "0px";
                    options.style.marginBottom = "0px";
                }
                else {
                    options.style.maxHeight = `${height}px`;
                    options.style.marginBottom = "5px";
                }
            }, 20);
        });
        const currentSortOrder = doc.querySelector("input[name=\"sortOrder\"]:checked");
        if (currentSortOrder.value === "1") {
            currentSortOrder.dispatchEvent(new Event("change"));
        }
        if (doc.getElementById("autoreload").checked) {
            doc.getElementById("autoreload").dispatchEvent(new Event("change"));
        }
        if (doc.getElementById("showheat").checked) {
            doc.getElementById("showheat").dispatchEvent(new Event("change"));
        }
        this.lazyLoad.then(LL => {
            new LL({
                data_src: "original",
                elements_selector: ".lazy",
                threshold: 10,
                use_native: true,
            });
        }).catch(err => {
            win.console.error(err);
        });
        if (this.popupManager.enabled)
            doc.querySelectorAll(".res1").forEach(elem => this.popupManager.attachToElement(elem));
        
        this.updateCurrentTabContentHeight();
        if (("ontouchstart" in win) && ("orientation" in win) && ("visualViewport" in win)) {
            win.addEventListener("touchstart", e => {
                if (win.visualViewport.offsetTop !== 0) {
                    return;
                }
                this.touchStartY = e.touches[0].pageY;
                this.touchMoveY = this.touchStartY;
                this.originalScale = win.visualViewport.scale;
                win.addEventListener("touchend", e => {
                    win.setTimeout(() => {
                        const scale = win.visualViewport.scale;
                        const threshold = /*win.screen.height * 0.15*/ 150 / scale;
                        if (!this.mobileReloadTimer && win.visualViewport.offsetTop <= 0 && scale === this.originalScale && this.touchMoveY > this.touchStartY + threshold) {
                            this.mobileReloadTimer = win.setTimeout(() => {
                                this.mobileReloadTimer = null;
                            }, 3000);
                            doc.querySelector(".menu a").click();
                        }
                    }, 10);
                }, {once: true});
            });
            win.addEventListener("touchmove", e => {
                this.touchMoveY = e.touches[0].pageY;
            });
        }
        doc.getElementById("ngform")?.insertAdjacentHTML("beforebegin", "<input type='checkbox' id='omitIsolated'>隔離スレを検索しない");
        const toggleOmitIsolated = doc.getElementById("omitIsolated");
        toggleOmitIsolated.style.marginLeft = "10px";
        toggleOmitIsolated.checked = this.omitIsolated;
        toggleOmitIsolated.addEventListener("change", event => {
            this.omitIsolated = event.target.checked;
            this.setValueToStorage("fcg_omitIsolated", this.omitIsolated ? 1 : 0);
            let words = doc.getElementById("searchwords").value;
            if (words) this.doSearch(words);
        });
        const toggleThemeButton = doc.createElement("a");
        toggleThemeButton.href = "javascript:void(0);";
        toggleThemeButton.textContent = (this.isDarkMode ? "ライト" : "ダーク") + "モードにする";
        toggleThemeButton.style.marginLeft = "10px";
        toggleThemeButton.addEventListener("click", event => {
            event.preventDefault();
            this.setValueToStorage("fcg_darkmode", this.isDarkMode ? 0 : 1);
            if (this.isDarkMode) {
                this.isDarkMode = false;
                doc.documentElement.classList.remove("dark-theme");
                event.currentTarget.textContent = "ダークモードにする";
            }
            else {
                this.isDarkMode = true;
                doc.documentElement.classList.add("dark-theme");
                event.currentTarget.textContent = "ライトモードにする";
            }
            doc.getElementById("showheat").dispatchEvent(new Event("change"));
        });
        doc.getElementById("ngform")?.insertAdjacentElement("beforebegin", toggleThemeButton);
        const toggleButton = doc.createElement("a");
        toggleButton.href = "javascript:void(0);";
        toggleButton.textContent = "レガシースクリプトを使用";
        toggleButton.style.marginLeft = "10px";
        toggleButton.addEventListener("click", event => {
            event.preventDefault();
            this.setValueToStorage("fcg_legacy", 1);
            this.window.location.reload();
        });
        doc.getElementById("ngform")?.insertAdjacentElement("beforebegin", toggleButton);
    }
    initLegacy() {
        const win = this.window;
        const doc = this.document;
        doc.documentElement.classList.remove("dark-theme");
        Promise.all([
            loadScript("https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js", doc)
            .then(() => loadScript("https://cdn.jsdelivr.net/npm/jquery.cookie@1.4.1/jquery.cookie.min.js", doc)),
            loadScript("https://cdn.jsdelivr.net/npm/object-fit-images@3.2.4/dist/ofi.min.js", doc)
        ]).then(() => loadScript("resources/script.js", doc))
        .catch(err => win.console.error(err));
        new Promise(resolve => {
            if (doc.readyState === "loading") {
                win.addEventListener("DOMContentLoaded", (event) => {
                    resolve();
                }, {once: true});
            } else {
                resolve();
            }
        }).then(() => {
            const toggleButton = doc.createElement("a");
            toggleButton.href = "javascript:void(0);";
            toggleButton.textContent = "モダンスクリプトを使用";
            toggleButton.style.marginLeft = "10px";
            toggleButton.addEventListener("click", event => {
                event.preventDefault();
                this.setValueToStorage("fcg_legacy", 0);
                win.location.reload();
            });
            doc.getElementById("ngform")?.insertAdjacentElement("beforebegin", toggleButton);
        });
    }
    getValueFromStorage(key) {
        return this.window.localStorage.getItem(key);
    }
    setValueToStorage(key, value) {
        this.window.localStorage.setItem(key, value);
    }
    updateCurrentTabContentHeight() {
        const currentTabSelector = `.${this.document.querySelector("input[name=\"tab-radio\"]:checked").id}-content`;
        const currentTabContent = this.document.querySelector(currentTabSelector);
        const {paddingTop, paddingBottom} = this.window.getComputedStyle(currentTabContent);
        this.currentTabContentHeight = currentTabContent.clientHeight - parseFloat(paddingTop);
    }
    doSearch(text) {
        const win = this.window;
        const doc = this.document;
        if (threadData == null) {
            threadData = JSON.parse(doc.getElementById("json-data").text);
        }
        if (threadKeys == null) {
            threadKeys = Object.keys(threadData).sort((a, b) => {
                if (a < b) return -1;
                if (a > b) return 1;
                return 0;
            });
        }
        const targetTable = doc.getElementById("favorites");
        const fragment = doc.createDocumentFragment();
        let regex = null, words = null;
        let added = 0;
        let row;
        doc.querySelectorAll("td.hidden").forEach(elem => {
            elem.classList.remove("hidden");
        });
        while (targetTable.firstChild) targetTable.removeChild(targetTable.lastChild);
        doc.querySelector(".warn")?.remove();
        if (text.charAt(0) === '(') {
            try {
                regex = new RegExp(text, "i");
            } catch (err) {
                doc.getElementById("searchform").insertAdjacentHTML("afterend", '<div class="warn">正規表現に誤りがあります</div>');
                return;
            }
        }
        else {
            if (text.indexOf('|') !== -1) words = text.toLowerCase().split('|');
            else words = text.toLowerCase().split(/[ 　]/);
            if (words.length === 1 && words[0] === "") {
                doc.querySelector(".message")?.remove();
                targetTable.insertAdjacentHTML("beforebegin", '<div class="message" align="center">検索ワードが空です</div>');
                return;
            }
        }
        for (let tag of threadKeys) {
            let found = false;
            if (regex) {
                if (regex.test(threadData[tag])) found = true;
            }
            else {
                for (let word of words) {
                    if (threadData[tag].indexOf(word) !== -1) {
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                const column = doc.getElementById(tag);
                if (this.omitIsolated) {
                    if (column?.closest(".tab-4-content")) continue;
                }
                if (column) {
                    if (added % win.threadsPerRow === 0) {
                        row = doc.createElement("tr");
                        fragment.appendChild(row);
                    }
                    const dupColumn = column.cloneNode(true);
                    dupColumn.removeAttribute("id");
                    dupColumn.setAttribute("id-orig", column.id);
                    const img = dupColumn.querySelector("img");
                    //img?.setAttribute("loading", "lazy");
                    if (img?.classList.contains("lazy")) {
                        if (!img.src) img.src = img.getAttribute("data-original");
                        img.classList.remove("lazy");
                        img.removeAttribute("data-original");
                    }
                    row.appendChild(dupColumn);
                    column.classList.add("hidden");
                    added++;
                }
            }
        }
        doc.querySelector(".message")?.remove();
        if (added === 0) {
            targetTable.insertAdjacentHTML("beforebegin", '<div class="message" align="center">検索ワードに一致したスレはありません</div>');
        } else {
            targetTable.appendChild(fragment);
            targetTable.querySelectorAll(".res1").forEach(elem => this.popupManager.attachToElement(elem));
        }
    }
    doFilterWithBlocklist(text) {
        const win = this.window;
        const doc = this.document;
        if (threadData == null) {
            threadData = JSON.parse(doc.getElementById("json-data").text);
        }
        if (threadKeys == null) {
            threadKeys = Object.keys(threadData).sort((a, b) => {
                if (a < b) return -1;
                if (a > b) return 1;
                return 0;
            });
        }
        if (!text) return;
        let regex = null, words = null;
        doc.querySelector(".warn")?.remove();
        if (text.charAt(0) === '(') {
            try {
                regex = new RegExp(text, "i");
            } catch (err) {
                doc.getElementById("ngform").insertAdjacentHTML("afterend", '<div class="warn">正規表現に誤りがあります</div>');
                return;
            }
        }
        else {
            if (text.indexOf('|') !== -1) words = text.toLowerCase().split('|');
            else words = text.toLowerCase().split(/[ 　]/);
            if (words.length === 1 && words[0] === "") {
                return;
            }
        }
        threadKeys = threadKeys.filter(key => {
            let found = false;
            if (regex) {
                if (regex.test(threadData[key])) found = true;
            }
            else {
                for (let word of words) {
                    if (threadData[key].indexOf(word) !== -1) {
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                const elem = document.getElementById(key);
                if (elem) {
                    if (!elem.nextElementSibling && !elem.previousElementSibling?.classList.contains("thread")) {
                        elem.parentNode.remove();
                    }
                    else elem.remove();
                }
                document.querySelectorAll(`td[id-orig="${key}"`)?.forEach(elem => {
                    if (!elem.nextElementSibling && !elem.previousElementSibling?.classList.contains("thread")) {
                        elem.parentNode.remove();
                    }
                    else elem.remove();
                });
                this.deletedThreads++;
            }
            return !found;
        });
    }
    fixupTable() {
        try {
            let tables = this.document.querySelectorAll(".nom.sortable");
            let numColsMax = this.window.threadsPerRow + 1;
            for (let table of tables) {
                let targetRow = null;
                let rowsToRemove = [];
                for (let tableRow of table.querySelectorAll("tr")) {
                    if (targetRow) {
                        let threadCols = tableRow.querySelectorAll(".thread");
                        for (let column of threadCols) {
                            targetRow.appendChild(column);
                            if (targetRow.childElementCount === numColsMax) {
                                targetRow = null;
                                break;
                            }
                        }
                    }
                    if (tableRow.childElementCount === 1) rowsToRemove.push(tableRow);
                    else if (tableRow.childElementCount < numColsMax) targetRow = tableRow;
                }
                rowsToRemove.forEach(row => row.remove());
            }
            this.document.querySelector("#apply_rules").insertAdjacentHTML("afterend", `
                <span style="font-style: italic">現在 ${this.deletedThreads} 個のスレッドが非表示です</span>
            `);
        } catch (e) {
            console.error(e);
        }
    }
}

if (!("jQuery" in window)) {
    const fcg = new FCG(window);
}
}());
