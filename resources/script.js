(function () {
var threadData;
var threadKeys;
var tabContentHeight = 0;
var useDynamicLazyload = 0;
var useAjaxReloadingOnServer = true;

if (!('lazyload' in $.fn)) {
	useDynamicLazyload = 1;
	if ('IntersectionObserver' in window) {
		var script = document.createElement('script');
		script.src = 'https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.min.js';
		script.onload = function() {
			new LazyLoad(document.querySelectorAll(".lazy"), {
				src: 'data-original'
			});
		}
		document.body.appendChild(script);
	} else {
		var script = document.createElement('script');
		script.src = 'https://cdn.jsdelivr.net/npm/jquery-lazyload@1.9.7/jquery.lazyload.min.js';
		script.onload = function() {
			$('img.lazy').lazyload();
		}
		document.body.appendChild(script);
	}
}

function doSearch()
{
	if(threadData == null) {
		threadData = $.parseJSON($('#json-data').text());
	}
	if(threadKeys == null) {
		threadKeys = Object.keys(threadData).sort(function(a,b){
			if( a < b ) return -1;
			if( a > b ) return 1;
			return 0;
		});
	}
	var text = $('#searchwords').val();
	var regex = null, words = null;
	var added = 0;
	var row;
	$('td.hidden').removeClass('hidden');
	$('#favorites').empty();
	$('.warn').remove();
	if(text.charAt(0) == '(') {
		try {
			regex = new RegExp(text,'i');
		} catch(error) {
			$('<div class="warn">正規表現に誤りがあります</div>').insertAfter('#searchform');
			return;
		}
	}
	else {
		if(text.indexOf('|') != -1) words = text.toLowerCase().split('|');
		else words = text.toLowerCase().split(/[ 　]/);
		if(words.length == 1 && words[0] == '') {
			$('.message').remove();
			$('<div class="message" align="center">検索ワードが空です</div>').insertBefore('#favorites');
			return;
		}
	}
	for(var i=0,len=threadKeys.length;i<len;i++) {
		var tag = threadKeys[i];
		var found = 0;
		if(regex) {
			if(regex.test(threadData[tag])) found = 1;
		}
		else {
			for(var j=0,len2=words.length;j<len2;j++) {
				var word = words[j];
				if(threadData[tag].indexOf(word) != -1) {
					found = 1;
					break;
				}
			}
		}
		if(found) {
			if(added % threadsPerRow == 0) {
				row = $('<tr>');
				$('#favorites').append(row);
			}
			var column = $('#'+tag);
			var dupColumn = column.clone().removeAttr('id').attr('id-orig',column.attr('id'));
			var lazy = dupColumn.find('.lazy');
			if(lazy) {
				lazy.attr({'src':lazy.attr('data-original')});
				lazy.removeAttr('class');
				lazy.removeAttr('data-original');
			}
			dupColumn.appendTo(row);
			column.addClass('hidden');
			added++;
		}
	}
	$('.message').remove();
	if(added == 0) {
		$('<div class="message" align="center">検索ワードに一致したスレはありません</div>').insertBefore('#favorites');
	}
}

function doBlacklisting()
{
	if(threadData == null) {
		threadData = $.parseJSON($('#json-data').text());
	}
	if(threadKeys == null) {
		threadKeys = Object.keys(threadData).sort(function(a,b){
			if( a < b ) return -1;
			if( a > b ) return 1;
			return 0;
		});
	}
	var text = $('#ngwords').val();
	var regex = null, words = null;
	if(text == null) return;
	if(text.charAt(0) == '(') {
		try {
			regex = new RegExp(text,'i');
		} catch(error) {
			$('<div class="warn">正規表現に誤りがあります</div>').insertAfter('#ngform');
			return;
		}
	}
	else {
		if(text.indexOf('|') != -1) words = text.toLowerCase().split('|');
		else words = text.toLowerCase().split(/[ 　]/);
		if(words.length == 1 && words[0] == '') {
			return;
		}
	}
	threadKeys = threadKeys.filter(function(element) {
		var found = 0;
		if(regex) {
			if(regex.test(threadData[element])) found = 1;
		}
		else {
			for(var j=0,len2=words.length;j<len2;j++) {
				var word = words[j];
				if(threadData[element].indexOf(word) != -1) {
					found = 1;
					break;
				}
			}
		}
		if(found) {
			$('#'+element).remove();
			$('td[id-orig="'+element+'"]').remove();
		}
		return (found == 0);
	});
}

function setupPopupPreview(objs)
{
	objs.hover(function(e) {
		if(!$('#preview').prop('checked')) return;
		var offset = $(this).offset();
		var tag = $(this).parent().attr('id');
		if(tag == null) tag = $(this).parent().attr('id-orig');
		if(tag == null) return;
		var tooltip = $(this).children('#tooltip_' + tag);
		if(tooltip.length) {
			tooltip.remove();
			return;
		}
		clearTimeout($(this).data('timeout'));
		tooltip = $('<div class="tooltip" id="tooltip_' + tag + '"></div>');
		tooltip.css({
			top: e.pageY - 83,
			left: e.pageX + 15
		});
		$(this).mousemove(function(e) {
			tooltip.css({
				top: e.pageY - 83,
				left: e.pageX + 15
			});
		});
		$(this).append(tooltip);
		var t = setTimeout(function(){
			tooltip.parent().unbind('mousemove');
			if(threadData == null) {
				threadData = $.parseJSON($('#json-data').text());
			}
			var link = tooltip.parent().children('a').attr('href');
			var imgsrc = tooltip.parent().find('img').attr('src');
			if(imgsrc) imgsrc = imgsrc.replace('/cat/', '/thumb/');
			else imgsrc = '';
			var thumb = $('<a href=\"' + link + '\"><img class="tooltip_thumb" src="' + imgsrc + '"></a>');
			var cow = tooltip.parent().children('.cow').clone();
			cow.css('font-size', '0.9em');
			var note = $('<div class="tooltip_note"></div>').append(cow);
			var pattern = link.match(/(https?:\/\/\w+\.2chan\.net\/)(\w+)\/res\/(\d+)\.htm/);
			if(pattern) {
				note.append($('<a class=\"delbutton\" href="' + pattern[1] + 'del.php?b=' + pattern[2] +'&d=' + pattern[3] + '"><i class="fas fa-trash"></i></a>'));
			}
			tooltip.append(thumb).append($('<div>', {class:'tooltip_text', text:threadData[tag]})).append(note);
			var tooltipX = parseInt(tooltip.css('left'));
			var tooltipY = parseInt(tooltip.css('top'));
			var overflowX = $(window).scrollLeft() - (tooltipX + tooltip.width() + 16 - $(window).width());
			var overflowTop = $(window).scrollTop() - tooltipY;
			var overflowBottom = $(window).scrollTop() - (tooltipY + tooltip.height() + 16 - $(window).height());
			if(overflowX < 0) {
				tooltip.css('left', tooltipX + overflowX - 10);
			}
			if(overflowTop > 0) {
				tooltip.css('top', tooltipY + overflowTop + 10);
			}
			if(overflowBottom < 0) {
				tooltip.css('top', tooltipY + overflowBottom - 10);
			}
			objectFitImages(thumb.children('img'));
			tooltip.fadeIn(200);
		},700);
		$(this).data('timeout', t);
	}, function() {
		if(!$('#preview').prop('checked')) return;
		var tag = $(this).parent().attr('id');
		if(tag == null) tag = $(this).parent().attr('id-orig');
		if(tag == null) return;
		var tooltip = $('#tooltip_' + tag);
		tooltip.parent().unbind('mousemove');
		tooltip.fadeOut(200).queue(function() {
			$(this).remove();
		});
		clearTimeout($(this).data('timeout'));
	});
}

function getValueFromCookieOrStorage(key)
{
	var value;
	if(typeof localStorage !== 'undefined') {
		value = localStorage.getItem(key);
	}
	if(value == null) value = $.cookie(key);
	return value;
}

function setValueToCookieAndStorage(key, value)
{
	$.cookie(key,value,{ expires: 365, path: '/' });
	if(typeof localStorage !== 'undefined') {
		localStorage.setItem(key, value);
	}
}

$(function() {
	var words = getValueFromCookieOrStorage('fcg_searchwords');
	var ngwords = getValueFromCookieOrStorage('fcg_ngwords');
	var sortOrder = getValueFromCookieOrStorage('fcg_sortorder');
	var autoReload = getValueFromCookieOrStorage('fcg_autoreload');
	var preview = getValueFromCookieOrStorage('fcg_preview');
	var showHeat = getValueFromCookieOrStorage('fcg_showheat');
	var timer;
	if(ngwords != null) {
		$('#ngwords').val(ngwords);
		doBlacklisting();
	} else $('#ngwords').val('');
	if(words != null) {
		$('#searchwords').val(words);
		doSearch();
	} else $('#searchwords').val('');
	if(sortOrder != null) {
		if(parseInt(sortOrder) == 1) {
			$('input[name="sortOrder"]').prop('checked', false);
			$('input[name="sortOrder"][value="1"]').prop('checked', true);
		}
	}
	if(autoReload != null) {
		if(parseInt(autoReload) == 1 && nextUpdate) {
			$('#autoreload').prop('checked', true);
		}
	}
	if(preview != null) {
		if(parseInt(preview) == 1) {
			$('#preview').prop('checked', true);
		}
	}
	if(showHeat != null) {
		if(parseInt(showHeat) == 1) {
			$('#showheat').prop('checked', true);
		}
	}
	if(useAjaxReloadingOnServer && location.protocol.indexOf('http') == 0) {
		nextUpdate = 0;
		var tmp = $('#autoreload').get(0);
		tmp.parentNode.removeChild(tmp.nextSibling);
		$('#autoreload').prop('checked', false).prop('disabled', true).hide();
		$('div.menu a').first().attr('href', 'javascript:void(0);').click(function() {
			$(this).hide();
			$(this).after('<span id="ajaxmsg">更新中...</span>');
			$.ajax({
				url: location.href,
				type: 'HEAD',
				timeout: 10000
			}).done(function(data, textStatus, jqXHR) {
				var dateCurrent = new Date(document.lastModified);
				var dateNew = new Date(jqXHR.getResponseHeader('Last-Modified'));
				if(dateNew > dateCurrent) location.reload();
				else {
					$('#ajaxmsg').text('更新なし');
					setTimeout(function() {
						$('#ajaxmsg').prev().show();
						$('#ajaxmsg').remove();
					},1500);
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('#ajaxmsg').text('通信エラー ('+textStatus+')');
				setTimeout(function() {
					$('#ajaxmsg').prev().show();
					$('#ajaxmsg').remove();
				},1500);
			});
		});
	}
	$('#searchform').submit(function() {
		setValueToCookieAndStorage('fcg_searchwords', $('#searchwords').val());
		doSearch();
		$('#tab-1').prop('checked', true);
		setupPopupPreview($('table#favorites div.res1'));
		tabContentHeight = $('.'+$('input[name="tab-radio"]:checked').attr('id')+'-content table').outerHeight(true);
		return false;
	});
	$('#ngform').submit(function() {
		var text = $('#ngwords').val();
		$('.warn').remove();
		if(text.charAt(0) == '(') {
			try {
				var regex = new RegExp(text,'i');
			} catch(error) {
				$('<div class="warn">正規表現に誤りがあります</div>').insertAfter('#ngform');
				return false;
			}
		}
		setValueToCookieAndStorage('fcg_ngwords', text);
		location.reload();
		return false;
	});
	$('input[name="sortOrder"]').change(function() {
		if($(this).val() == '1') {
			setValueToCookieAndStorage('fcg_sortorder', 1);
			$('table.sortable tr').each(function(i, tr) {
				var sorted = $('td.thread', tr).sort(function(a, b) {
					return $(b).data('length') - $(a).data('length');
				});
				$(tr).append(sorted);
			});
		} else {
			setValueToCookieAndStorage('fcg_sortorder', 0);
			$('table.sortable tr').each(function(i, tr) {
				var sorted = $('td.thread', tr).sort(function(a, b) {
					var vA = $('a', a).attr('href');
					var vB = $('a', b).attr('href');
					return (vA < vB) ? 1 : (vA > vB) ? -1 : 0;
				});
				$(tr).append(sorted);
			});
		}
	});
	$('#autoreload').change(function() {
		if(!nextUpdate) return;
		if($(this).prop('checked')) {
			setValueToCookieAndStorage('fcg_autoreload', 1);
			if(timer == null) {
				var timeToReload = nextUpdate - (new Date()).getTime();
				timer = setTimeout(function () {
					location.reload();
				}, timeToReload > 0 ? timeToReload : 60000);
			}
		} else {
			setValueToCookieAndStorage('fcg_autoreload', 0);
			if(timer != null) {
				clearTimeout(timer);
				timer = undefined;
			}
		}
	});
	$('#preview').change(function() {
		if($(this).prop('checked')) {
			setValueToCookieAndStorage('fcg_preview', 1);
		} else {
			setValueToCookieAndStorage('fcg_preview', 0);
		}
	});
	$('#showheat').change(function() {
		if($(this).prop('checked')) {
			setValueToCookieAndStorage('fcg_showheat', 1);
			$('td.thread[heatbg]').each(function(i, td) {
				$(td).css('background-color',$(td).attr('heatbg'));
			});
		} else {
			setValueToCookieAndStorage('fcg_showheat', 0);
			$('td.thread[heatbg]').each(function(i, td) {
				$(td).css('background-color','');
			});
		}
	});
	$('input[name="tab-radio"]').change(function() {
		var contentClass = $(this).attr('id') + '-content';
		var content = $('.'+contentClass);
		var newTabContentHeight = content.find('table').outerHeight(true);
		var contentTable = $('.'+contentClass+' table');
		var duration = 200;
		if(Math.abs(newTabContentHeight - tabContentHeight) < 10) duration = 50;
		contentTable.css('display','none');
		content.css('padding-bottom',10+tabContentHeight+'px');
		content.animate({
			'padding-bottom': 10+newTabContentHeight+'px'
		},{
			'duration': duration,
			'complete': function(){
				content.css('padding-bottom','');
				contentTable.css('display','table');
				content.find('img[src-original]').each(function(i, img) {
					var lazyimg = $(img);
					lazyimg.attr({'src':lazyimg.attr('src-original')});
					lazyimg.removeAttr('src-original');
				});
			}
		});
		tabContentHeight = newTabContentHeight;
	});
	$('#option-button').click(function() {
		$('div.options').slideToggle(200);
	});
	if($('input[name="sortOrder"]:checked').val() == '1') $('input[name="sortOrder"]:checked').trigger('change');
	if($('#autoreload').prop('checked')) $('#autoreload').trigger('change');
	if($('#showheat').prop('checked')) $('#showheat').trigger('change');
	if(!useDynamicLazyload) $('img.lazy').lazyload();
	setupPopupPreview($('div.res1'));
	tabContentHeight = $('.'+$('input[name="tab-radio"]:checked').attr('id')+'-content table').outerHeight(true);
});

}());
